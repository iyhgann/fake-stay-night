transform chan_tf:
    zoom 0.8
    yalign 0.5

transform chan_tf_subs:
    zoom 0.8
    xalign 0.8
    yalign 0.5

image 3dgchan_crying_face:
    "3dgchan_f8"
    block:
        yoffset 0.0
        linear 0.1 yoffset 1.2
        repeat

image 3dgchan oligofrenic = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f0.png")
image 3dgchan greet = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_b1.png",
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_f1.png")
image 3dgchan = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f1.png")
image 3dgchan presenting = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_b4.png",
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_f1.png")
image 3dgchan presenting smiling = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_b4.png",
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_f7.png")
image 3dgchan wtf = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f6.png")
image 3dgchan smiling = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f7.png")
image 3dgchan disgusted = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f5.png")
image 3dgchan moved = LiveComposite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "3dgchan_crying_face")
image 3dgchan angry = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f9.png")
image 3dgchan bothered = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f2.png")
image 3dgchan hair= Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b2.png",
    (0, 0), "characters/3dgchan/3dgchan_f1.png")
image 3dgchan bothered hair= Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b2.png",
    (0, 0), "characters/3dgchan/3dgchan_f2.png")
image 3dgchan laughing hair = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b2.png",
    (0, 0), "characters/3dgchan/3dgchan_f3.png")
image 3dgchan laughing = Composite(
    (811, 960),
    (0, 0), "characters/3dgchan/3dgchan_body.png",
    (0, 0), "characters/3dgchan/3dgchan_b3.png",
    (0, 0), "characters/3dgchan/3dgchan_f3.png")
