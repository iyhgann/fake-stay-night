transform x_tf:
    zoom 0.8
    yalign -0.5

# image sweet 0 = LiveComposite(
#         xanchor=0.5 yanchor=1.0 # << This is the gist of what I tried, it threw up exceptions, worked fine again with this line removed
#         (245, 434),
#         (0, 0), "sweet.png", #the character sprite
#         (100, 115), Animation(Null(), 2.7, "sweetblink.png", .3) , # a little tile of closed-eyes that I patch over the open ones
#         (150, 173), WhileSpeaking("sweet", Animation(Null(), .08, "sweetmouth1.png", .08), Null()), # lip-flap (lazy, two frames :D )
#         )

image x_head = Composite(
    (563, 959),
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_closed.png"
)

image x seeyousoon = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 2.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 2.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_closed.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 2.5)
)

image x dark = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_default.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_bigote.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5)
)

image x dark blur = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_default.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5)
)

image x dark closed blur = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_closed.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.2)), 1.5),
)

image x dark story = Composite(
    (563, 959),
    (0, 0), im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_f_default.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3))
)

image x dark screaming = Composite(
    (563, 959),
    (0, 0), im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_f_screaming.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3))
)

image x dark remembering = Composite(
    (563, 959),
    (0, 0), im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_f_remembering.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3))
)

image x dark closed = Composite(
    (563, 959),
    (0, 0), im.MatrixColor("characters/x/x_body.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_head.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3)),
    (0, 0), im.MatrixColor("characters/x/x_f_closed.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0, 0.3))
)

image x fire = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png",  im.matrix.tint(0.619, 0.223, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.tint(0.619, 0.223, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_default.png", im.matrix.tint(0.619, 0.223, 0)), 0.1)
)

image x fire screaming = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png",  im.matrix.tint(0.619, 0.223, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.tint(0.619, 0.223, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_screaming.png", im.matrix.tint(0.619, 0.223, 0)), 0.1)
)

image x black = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png",  im.matrix.tint(0.05, 0.0, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.tint(0.05, 0.0, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_default.png", im.matrix.tint(0.05, 0.0, 0)), 0.1)
)

image x black remembering = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png",  im.matrix.tint(0.05, 0.0, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.tint(0.05, 0.0, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_remembering.png", im.matrix.tint(0.05, 0.0, 0)), 0.1)
)

image x black closed = Composite(
    (563, 959),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_body.png",  im.matrix.tint(0.05, 0.0, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_head.png", im.matrix.tint(0.05, 0.0, 0)), 0.1),
    (0, 0), im.Blur(im.MatrixColor("characters/x/x_f_closed.png", im.matrix.tint(0.05, 0.0, 0)), 0.1)
)

image x unshaven = Composite(
    (563, 959),
    (0, 0), "characters/x/x_body.png",
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_default.png",
    (0, 0), "characters/x/x_bigote.png")

image x unshaven huh = Composite(
    (563, 959),
    (0, 0), "characters/x/x_body.png",
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_huh.png",
    (0, 0), "characters/x/x_bigote.png")

image x = Composite(
    (563, 959),
    (0, 0), "characters/x/x_body.png",
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_default.png")

image x closed = Composite(
    (563, 959),
    (0, 0), "characters/x/x_body.png",
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_closed.png")

image x remembering = Composite(
    (563, 959),
    (0, 0), "characters/x/x_body.png",
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_remembering.png")

image x screaming = Composite(
    (563, 959),
    (0, 0), "characters/x/x_body.png",
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_screaming.png")

image x smiling = Composite(
    (563, 959),
    (0, 0), "characters/x/x_body.png",
    (0, 0), "characters/x/x_head.png",
    (0, 0), "characters/x/x_f_smiling.png")
