image fade = Solid("#000")
# image story_scene = Solid("#040312")

style commandment_style is text:
    # font "fonts/AUGUSTUS.ttf"
    font augustus_font
    text_align 0.5
    size 40

label story:
    # FOTO FORO

    user"podrías explicarme al menos porque todos me dicen Jerikito? que Jerikito ésto, que Jerikito aquello!!!"

    x"callate Jerikito, por favor no rompas las bolas T_T"

    user"Incluso vos!!! Estoy harto, todo esto sumado a los sueños, me estoy volviendo loco"

    x"sueños{nw}"
    show x closed onlayer screens
    extend"{w=0.5}.{w=0.5}.{w=0.5}. esta empezando a cruzarse a la realidad."

    show siteinboxdark:
        alpha 1.0
        linear 3.0 alpha 0.0

    show x onlayer screens
    x"esto va a ser un post largo, [name].{w=0.5} presta atención:"
    scene black
    hide siteinboxgradient
    show x at x_tf onlayer screens:
        xalign 0.8
        linear 1.0 xalign 1.0

    show x dark story onlayer screens with dissolve

    x"partimos desde el comienzo de la humanidad,{nw}"
    user"historia?? naa qu{nw}"
    show x dark screaming onlayer screens
    x"{cps=0}partimos desde el comienzo de la humanidad,{/cps}{cps=*4} NO VOY A CONTARTE TODA LA HISTORIA." with hpunch
    show x dark story onlayer screens
    x"sabe que por todas las guerras sangre y tortura{w=0.5} nada de eso se compara con la ininmutable crueldad que la naturaleza impone sobre sus habitantes"

    x"es la {i}{color=[color_main]}RAZON{/color}{/i} la que nos permitió escapar del espiral de dolor."
    x"Los pueblos occidentales entendieron ésto y la establecieron como la base de su civilizacion"

    show forum:
        ypos -100
        xalign 0.0
        alpha 0.0
        zoom 0.5
        parallel:
            linear 15 rotate -20 xalign 0.2 ypos -100 zoom 1.0
            linear 3 zoom 2.0 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0

    show x dark blur onlayer screens with dissolve
    x"el mayor exponente es el {color=[color_main]}foro romano{/color}, plazas donde los ciudadanos creaban hilos de honesta discusión..."
    x"es ahí donde las opiniones que estos \"posteaban\" se transmitieron de boca en boca, y tejieron un manto de razón que cubrio el mundo entero"

    hide forum with Dissolve(1.0)

    show medicine:
        xalign 0.0
        alpha 0.0
        zoom 0.8
        parallel:
            linear 4.0 xalign 0.2 yalign 0.5 zoom 1.0
            linear 3 zoom 1.2 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0
    x"la medicina?{w=1.0}"
    show roman_forum:
        xalign 1.0
        alpha 0.0
        zoom 0.8
        parallel:
            linear 4.9 xalign 0.2 yalign 0.5 zoom 1.0
            linear 3 zoom 1.2 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0
    extend" la moral y la leyes?{w=1.0}"
    show christianity:
        xalign 0.0
        alpha 0.0
        zoom 0.8
        parallel:
            linear 5.0 xalign 0.2 yalign 0.5 zoom 1.0
            linear 3 zoom 1.2 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0
    extend" la religion cristiana?"

    x"pueden ser vistos como lo que realmente son:"
    play sound "audio/impact_1.ogg"
    x"{w=.4}{i}{cps=0}{color=[color_main]}{size=+10}TRETS{/size}{/color}{/i}{/cps}{w=1.0}, de cientos de miles de páginas{p}trets que se extienden a lo largo de generaciones"

    user"{i}guauu{/i}"

    play sound "audio/punch_echo.ogg"
    show x dark screaming onlayer screens with Dissolve(0.2)
    if sex == SEX_FEMALE:
        x"{size=+8}{cps=*5.0}DEBES ESTAR REVISANDO {i}IG{/i} NO TUNUDA??\nPRESTA ATENCION!!" with hpunch
    else:
        x"{size=+8}{cps=*5.0}ESTAS JUGANDO {i}GACHA LIFE{/i} NO, PELOTUDO??\nMIRA LA PANTALLA!!" with hpunch

    show x dark story onlayer screens

    x"nos adelantamos a casi el presente {nw}"

    show internet_2:
        xalign 1.0
        alpha 0.0
        zoom 0.8
        parallel:
            linear 5.0 xalign 0.2 yalign 0.5 zoom 1.0
            linear 3 zoom 1.2 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0

    play sound 'audio/internet.ogg'

    show internet:
        xalign 0.0
        alpha 0.0
        zoom 0.4
        parallel:
            pause 1.0
            linear 5.0 xalign 0.2 yalign 0.5 zoom 0.5
            linear 3 zoom 1.2 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0

    show x dark blur onlayer screens with Dissolve(0.2)
    extend "con la {i}invención{/i} del {color=[color_main]}internet{/color}"
    x"una red de pornografía cuya cualidad emergente es la transimision de informacion"
    x"es aqui donde prolifero una nueva y posible mejor version del antiguo foro"

    play sound 'audio/crowd.ogg' fadein 5.0
    x"muchos creen que fue la vagancia de salir de un programador de PHP obeso...{w=2.0}{cps=*2.0}o alguien muy mangina para hablar con gente en la vida real"

    show vbulletin_2:
        xalign 0.0
        alpha 0.0
        zoom 1.4
        parallel:
            linear 8.0 xalign 0.2 yalign 0.5 zoom 0.5
            linear 3 zoom 0.4 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0
    x"el punto es que de un dia para el otro de la mano de software como {color=[color_main]}vBulletin{/color} florecieron las comunidades más estables de foros de internet"


    show psicofxp:
        xalign -0.2
        alpha 0.0
        parallel:
            ease_quad 2.0 xalign 0.1 alpha 1.0

    x"Psicofxp, {w}"

    show forocoches:
        xalign 1.5
        yalign 0.7
        alpha 0.0
        parallel:
            ease_quad 2.0 xalign 0.6 alpha 1.0

    extend"Forocoches"

    show foroescortsxp:
        xalign -0.2
        yalign 0.6
        alpha 0.0
        zoom 0.9
        parallel:
            ease_quad 2.0 xalign 0.1 alpha 1.0

    show x dark remembering onlayer screens with dissolve
    x"...{i}ForoescortsXP{/i}{cps=*5}{size=-10}*sigh*"

    hide foroescortsxp with Dissolve(0.2)
    hide forocoches with Dissolve(0.2)
    hide psicofxp with Dissolve(0.2)

    show x dark closed onlayer screens

    show 3dglegacy:
        xalign 0.4
        yalign 0.6
        alpha 0.0
        rotate 10
        zoom 2.2
        parallel:
            linear 10.0 xalign 0.1 zoom 0.8 rotate -20
        parallel:
            easein 1.0 alpha 1.0
            pause 6.0
            easeout 1.0 alpha 0.0

    x"y este foro."
    show x dark story onlayer screens
    x"por mucho tiempo el resultado fue positivo,{p}gente logrando su potencial,{p}leve indice de posts repetidos"
    hide 3dglegacy with dissolve
    x"fueron años de {i}{color=[color_main]}prosperidad{/color}{/i}{w=1.0}, y no habia prospecto de que las cosas cambiaran"

    hide vbulletin_2 with dissolve
    stop sound fadeout 2.0

    show x dark closed onlayer screens
    x"pero estamos aca verdad??{w} en el medio de la oscuridad. {w=2.0}{nw}"
    show x dark story onlayer screens
    extend"\n{cps=*2.0}alguna pregunta??"

    if choosed_porn:
        user"Tenes idea como conseguir el video porn{nw}"
    elif choosed_voxed:
        user"Voxed y 9gag tmb aparecieron en est{nw}"
    else:
        user"{cps=*5}Escuchaste BTS???, yo soy de la BTS arm-{nw}"

    show x dark screaming onlayer screens
    play sound "audio/ninja_hit.ogg"
    x"CERRA{nw}" with vpunch
    extend " LA{nw}" with hpunch
    extend " BOCA" with vpunch

    show x dark story onlayer screens
    x"{size=+10}ALGO SALIO MAL [name!u]"
    show x dark remembering onlayer screens
    x"algo salio muy mal"

    show x dark story onlayer screens
    x"que sucede cuando alguien descubre que puede ser mas de dos personas"
    x"que puede no solo tener multiples identidades, sino entrelazarlas en una cadena atemporal de boludez y confusión"

    $ renpy.pause(2.0, hard='True')
    x"Estoy hablando de {nw}"
    play sound "audio/impact_4.ogg"
    extend"{color=[color_main]}{size=+15}{cps=0}FAKES{/color}"
    $ renpy.pause(2.0, hard='True')

    show fake_1:
        xalign 0.5
        yalign 0.5
        zoom 0.4
        alpha 0.0
        parallel:
            linear 15.0 yalign 0.4 zoom 0.8
            linear 2.0 alpha 0.0
        parallel:
            easeout 2.0 alpha 1.0
    show fake_2:
        xalign 0.7
        yalign 0.7
        zoom 0.5
        alpha 0.0
        pause 1.0
        parallel:
            linear 12.0 xalign 0.9 yalign 0.9 zoom 1.2
            linear 2.0 alpha 0.0
        parallel:
            easeout 2.0 alpha 1.0
    show fake_3:
        xalign 0.1
        yalign 0.1
        zoom 0.3
        alpha 0.0
        pause 4.0
        parallel:
            linear 15.0 xalign 0.0 yalign 0.2 zoom 0.9
            linear 2.0 alpha 0.0
        parallel:
            easeout 2.0 alpha 1.0
    show fake_4:
        xalign 0.6
        yalign 0.2
        zoom 0.2
        alpha 0.0
        pause 2.0
        parallel:
            linear 13.0 xalign 0.9 yalign 0.3 zoom 0.6
            linear 2.0 alpha 0.0
        parallel:
            easeout 2.0 alpha 1.0
    show fake_5:
        xalign 0.2
        yalign 0.3
        zoom 0.3
        alpha 0.0
        pause 3.0
        parallel:
            linear 15.0 xalign 0.3 yalign 0.4 zoom 0.7
            linear 2.0 alpha 0.0
        parallel:
            easeout 2.0 alpha 1.0

    show x dark blur onlayer screens with Dissolve(0.2)
    x"trolls, fakes, usuarios falsos, como quieras llamarles, la idea es la misma"

    x"un fake es la peor expresion de un foro,"
    x"{cps=*2}es usar el anonimato, no para ser honesto, sino para obfuscar y confundir."
    x"{cps=*2}Es ante un fake que el anonimato se ve degradado,{w} incluso por sus propios pares, que emigran a otros sitios, ironicamente {i}aun mas fakes{/i} que este forito"

    show x dark story onlayer screens with dissolve
    user"Cuál es el problema de un fake?,{w} alguien se crea una cuenta falsa, los ignoras y listo!!! no veo cual es el drama :P"

    show x dark closed onlayer screens
    x"pero {i}vos sabes cual es el drama,{w=0.5} verdad, [name!i]?{/i}"

    user"..."

    x"dale tonto, decilo..."

    user"El{w=0.5}.{w=0.5}.{w=0.5}.{w=2.0}{color=[color_main]}{cps=*0.5}{i}Ban???{/i}{/cps}{w=2.0}{nw}"

    user"{cps=0}El...{color=[color_main]}Ban{/color}{/cps}{cps=*2}{color=[color_main]} De Banes{/color}{/cps}"

    x"exacto, el Ban De Banes,"
    show ban:
        xalign 0.2
        alpha 0.0
        zoom 2.2
        parallel:
            linear 20.0 xalign 1.0 yalign 1.0 zoom 1.8
            linear 3 zoom 1.7 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0

    show x dark closed blur onlayer screens with dissolve
    extend" un evento en el que converge el declive de foros php no moderados con la decadencia y final de la civilizacion"

    x"la verdad es, [name], {i}estamos siendo puestos a prueba{/i}"

    x"estos foritox™ son mucho mas que un lugar banal donde pasar el tiempo.{p}Los foros son...{w=1.0}{i}eran{/i} nuestra ultima oportunidad"
    x"la oportunidad de probar nuestro uso de la razon"
    x"de superarnos y crecer contra la decadencia"
    x"por sobretodas las cosas, la oportunidad de{w=0.5} dadas las cirscunstancias,{w=0.5} no spamear con cuentas fakes de mierda"

    hide ban with dissolve
    show x dark story at x_tf onlayer screens:
        xalign 1.0
        linear 1.0 xalign 0.5

    show x onlayer screens with dissolve

    stop ambient fadeout 5.0
    x"espero que estes empezando a comprender la gravedad de la situacion"

    if sex == SEX_MALE:
        user"cuando hice la catequesis...{w}{cps=*2}o sea ahora soy {i}ateo{/i} obviamente, jaja Dios, como alguien puede creer que hay un viejito con barba"
    else:
        user"cuando hice la catequesis...{w}{cps=*2}o sea ahora soy {i}atea{/i} obviamente, jaja Dios, como alguien puede creer que hay un viejito con barba"

    user"{cps=*2}{i}I fucking love science!!!{/i}{/cps}{w} pero recuerdo haber leido de los pecados capitales..."
    user"no se supone que seriamos juzgados por esos, los 10 mandados digo"
    user"(no es que crea en dios obv-{nw}"

    show x screaming onlayer screens
    x"ya!{w=0.5}{nw}" with hpunch
    show x closed onlayer screens
    extend " es correcto lo que decis.{w=1.0} los 10 mandamientos son las reglas por las cuales habríamos de ser juzgados"

    show x onlayer screens
    x"lo que tenes que entender es lo siguiente:"
    xh"{size=+5}{i}{font=fonts/AUGUSTUS.ttf}No ser fake{/font}{w=0.2}{nw}"
    play sound "audio/impact_2.ogg"
    extend"{cps=*5} es la suma de los 10 mandamientos{/i}{/size}"

    x"es el {color=[color_main]}mandamiento final."

    user"como???"

    jump decalogue
