image site_nav:
    "sitenav"
    zoom 0.675
    xcenter 0.5
    ypos -0.005

image site_nav_logged:
    "sitenavlogged"
    zoom 0.675
    xcenter 0.5
    ypos -0.005

image site_main:
    "sitemain"
    zoom 0.60
    xalign 0.5
    ycenter 0.5

image mario = Movie(play="bg/mario.webm", mask="bg/mario_mask.webm")

transform mario_move:
    xalign -0.2
    choice:
        pause 30.0
    choice:
        pause 10.0
    choice:
        pause 5.0

    linear 10.0 xalign 1.2

screen mario_screen():
    add "mario"

style site_button:
    size 20
    xpadding 13
    ypadding 6
    background "#f1921a"
    hover_background "#f9ab32"
    #activate_sound "audio/accept.mp3"

style login_button:
    size 20
    xpadding 13
    ypadding 6
    outlines [ (10.,"#000000",0,0) ]
    hover_background "login_button_hover"
    activate_sound "audio/accept.mp3"

image login_button_hover = At(Solid("f9ab32"), sub_button_hover_transform)

style site_button_text:
    size 14.5
    font gui.text_font_bold
    kerning 0
    # outlines [ (absolute(1), "#f1921a", absolute(0), absolute(1)) ]
    # hover_outlines [ (absolute(1), "#32363c", absolute(0), absolute(1)) ]
    color "#fff"
    selected_color "#fff"
    idle_color "#fff"

image sub_button_hover = At(Solid("80d2ff"), sub_button_hover_transform)
transform sub_button_hover_transform:
    pause 0.2
    alpha 0.0
    linear 0.5 alpha 0.4
    linear 0.5 alpha 0.0
    repeat

style sub_button is button:
    xsize 980
    ysize 100
    hover_background "sub_button_hover"
    spacing 10

screen site_screen(logged=False, clicks=0, can_log=False):
    tag site
    zorder 0
    modal False

    side "c r":
        viewport id "vp_main":
            mousewheel True
            arrowkeys True

            frame:
                background Solid("#24262c")
                xfill True

                fixed:
                    xalign 0.5
                    yminimum 3116 #3136
                    add "site_main"

                    if logged:
                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 202
                            ysize 64
                            action Jump('site_label_bienvenidas')
                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 310
                            ysize 90
                            action Jump('site_label_compra_venta')
                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 445
                            ysize 50
                            action Jump('site_label_private')
                        #outlands
                        if clicks >= 3:
                            button:
                                style "sub_button"
                                xalign 0.5
                                yoffset 497
                                ysize 97
                                action Jump('site_label_discusion_general')
                            button:
                                style "sub_button"
                                xalign 0.5
                                yoffset 595
                                ysize 30
                                hover_background "#ffb84b44"
                                action Jump('site_label_outlands')
                        else:
                            button:
                                style "sub_button"
                                xalign 0.5
                                yoffset 497
                                ysize 130
                                action Jump('site_label_discusion_general')

                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 630
                            ysize 100
                            action Jump('site_label_empleos')
                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 731
                            ysize 88
                            action Jump('site_label_autos')
                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 825
                            ysize 105
                            action Jump('site_label_deportes')
                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 926
                            ysize 110
                            action Jump('site_label_cine')
                        button:
                            style "sub_button"
                            xalign 0.5
                            yoffset 1037
                            ysize 70
                            action Jump('site_label_sexualidad')

                        button:
                            add "mario" size (32.0, 38.0)
                            at mario_move
                            yoffset 2738
                            action Jump('mario_click')

        vbar value YScrollValue("vp_main"):
            unscrollable "hide"

    fixed:
        if logged:
            add "site_nav_logged"
        elif can_log:
            add "site_nav"
            button:
                style "login_button"
                xpos 1080
                ypos 0
                xsize 150
                ysize 60
                action Jump('site_register')
        else:
            add "site_nav"

style skin_button is button:
    xsize 200
    ysize 100

    spacing 10
    xmargin 10
    ymargin 20
    activate_sound "audio/menu_click.ogg"

screen skin_screen():
    modal True

    grid 3 3:
        xalign 0.5 yalign 0.5

        button:
            style "skin_button"
            background "#744718"
            hover_background "#ffb84b"
            action Return(True)
        button:
            style "skin_button"
            background "#1b1005"
            hover_background "#ffb84b"
            action Return(True)
        button:
            style "skin_button"
            background "#000000"
            hover_background "#ffb84b"
            action Return(True)

        button:
            style "skin_button"
            background "#8f863e"
            hover_background "#ffb84b"
            action Return(True)
        button:
            style "skin_button"
            background "#413c13"
            hover_background "#ffb84b"
            action Return(True)
        button:
            style "skin_button"
            background "#844d34"
            hover_background "#ffb84b"
            action Return(True)

        button:
            style "skin_button"
            background "#9d8872"
            hover_background "#ffb84b"
            action Return(True)
        button:
            style "skin_button"
            background "#564846"
            hover_background "#ffb84b"
            action Return(True)
        button:
            style "skin_button"
            background "#45351c"
            hover_background "#ffb84b"
            action Return(True)

image dialog_button_bg:
    Solid("#440000")
    alpha 0.5

screen db_screen(): #dialog button
    zorder 0
    modal False

    button:
        # background "dialog_button_bg" # if you comment out this line you'll see the button and the area it takes up
        xfill True # slightly smaller than the viewport area to make sure you don't click it by accident when fiddling with the scrollbar
        yfill True
        xpos -15
        ypos 60
        action Return() # returns from the screen -- in this context, that means it moves on to the next statement
