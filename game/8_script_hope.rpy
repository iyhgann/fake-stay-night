image dota2 = Movie(play="hope/dota2.webm", loop=False)

label hope:
    play ambient "audio/wind.ogg"
    show siteinboxgradient onlayer screens: #foreground
        alpha 0.5
        choice:
            linear 0.2 alpha 0.6
            linear 0.2 alpha 0.5
        choice:
            linear 4 alpha 0.8
            linear 4 alpha 0.5
        choice:
            linear 2 alpha 0.8
            linear 1 alpha 0.5
        repeat

    show siteinboxdark with dissolve

    show x at x_tf onlayer screens with dissolve:
        xalign 0.5

    x"ese {i}jerikito{/i}{w}, el que aparece en tus pesadillas"
    x"fue el peor de nosotros"
    x"fue...destruido{w=0.5} su...canal de youtube...{w=0.2}{nw}"
    user"TENÍA UN CANAL DE YOUTUBE{nw}" with hpunch
    show x remembering at x_tf onlayer screens
    extend"???"

    $ renpy.pause(2.0, hard='True')
    user"hace cuanto tiempo fue esto???"

    show x at x_tf onlayer screens
    if sex == SEX_MALE:
        x"el tiempo no pasa en outlands nene"
    else:
        x"el tiempo no pasa en outlands nena"

    x"cuantos rollback ocultan los hechos...si es que realmente paso algo"

    user"Pero..."
    show x at x_tf onlayer screens
    extend"yo que puedo hacer al respecto??{p}{cps=*3}{i}soy solo un adolescente de 28 años!!"

    show x smiling at x_tf onlayer screens
    x"{cps=*0.5}...{/cps}jajajaja, {w=0.2}"
    x"{i}hacer{/i}"
    show x at x_tf onlayer screens
    x"la batalla esta perdida"
    x"el {i}ban de banes{/i} es inminente e inevitable.{p}{i}preferible{/i} incluso, algunos afirmaran"

    hide x onlayer screens

    show x_body at x_tf onlayer screens:
        xalign 0.5
        yalign 0.22
        block:
            easein 0.2 rotate 0.5
            pause 10
            easein 0.5 rotate -0.5
            pause 10
            repeat
    show x_head onlayer screens:
        xalign 0.5
        zoom 0.8
        yalign 0.22
        block:
            ease_quad 0.5 rotate 1.0
            pause 5.0
            easeout 0.5 rotate -1.2
            pause 10.0
            repeat

    play sound "audio/hdd.ogg" fadein 2.0
    x"Ahh...puedo escuchar el ruido del RAID de discos ide usados resquebrajandose"
    x"los cabezales gastados por escribir threads inconsecuentes una{w=0.2} y otra vez{w=0.2}...y otra vez"
    stop sound fadeout 3.0

    hide x_body onlayer screens
    hide x_head onlayer screens

    show x at x_tf onlayer screens:
        xalign 0.5
    x"y por cierto,{w=0.2} que te hace pensar que te hubiera preguntado a {i}vos{/i} si pudieras resolver esto"
    x"probablemente le hubiera dicho, {w=0.5}{nw}"
    show x remembering at x_tf onlayer screens
    extend"qcyo,{w=0.5}{nw}"
    show x at x_tf onlayer screens
    extend" a {i}Okorn{/i} ponele"

    show siteinboxdark:
        alpha 1.0
        linear 3.0 alpha 0.0

    show x remembering onlayer screens
    x"y sin embargo{w=0.1}.{w=0.1}.{w=0.1}."

    show x onlayer screens:
        ease_cubic 2.0 xalign 0.9
    $ renpy.pause(4.0, hard='True')
    xh"{font=fonts/AUGUSTUS.ttf}{i}\"Aquél que es no con la grasa, aquel que ha de romper la maldicion "

    show gym:
        xalign 0.5
        yalign 0.5
        alpha 0.0
        zoom 0.5
        parallel:
            linear 15  zoom 1.0
            linear 3 zoom 2.0 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0

    show x dark blur onlayer screens with dissolve
    extend"andará, entonces, al {size=+8}{color=[color_main]}gym{/color}.{/i}"

    show vaso_ena:
        xalign 1.3
        yalign 0.5
        alpha 0.0
        zoom 1.0
        parallel:
            linear 15 rotate -40 zoom 2.0
            linear 3 zoom 2.2 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0
    show proteina:
        xalign -0.3
        yalign 0.5
        alpha 0.0
        zoom 0.8
        parallel:
            linear 15 xalign 0.1 rotate 20 zoom 1.8
            linear 3 zoom 2.2 alpha 0.0
        parallel:
            linear 3.0 alpha 1.0
    xh"{font=fonts/AUGUSTUS.ttf}{i}donde reunira fuerzas para subir escaleras, y destruir su vinculo con el delivery\"{/i}"
    $ renpy.pause(2.0, hard='True')

    hide gym with Dissolve(.2)
    hide vaso_ena with Dissolve(.2)
    hide proteina with Dissolve(.2)

    show dota2 zorder 0:
        xalign 0.5
        yalign 0.5
        alpha 0.0
        zoom 0.5
        parallel:
            linear 15 zoom 2.0
            linear 3 zoom 2.2 alpha 0.0
        parallel:
            ease_quad 4.0 alpha 1.0
    xh"{font=fonts/AUGUSTUS.ttf}{i}\"Fueron 9000,{w=0.5} las horas de batalla en la Valhalla.\"{/i}"

    show vladi zorder 1:
        xalign 0.0
        yalign 0.5
        alpha 0.0
        zoom 1.5
        parallel:
            linear 15 zoom 2.0 xalign 0.0
            linear 3 zoom 2.2 alpha 0.0
        parallel:
            ease_quad 6.0 alpha 0.4

    xh"{font=fonts/AUGUSTUS.ttf}{i}\"Allí conoció la victoria frente a peruanos y chilenos,\nhaciéndolos echar de sus casas por no poder controlar su furia.\"{/i}"
    $ renpy.pause(2.0, hard='True')
    hide vladi with Dissolve(.2)
    hide dota2 with Dissolve(.2)

    xh"{font=fonts/AUGUSTUS.ttf}{i}\"A la hora de ascender"
    show rue_1 zorder 1:
        xalign 0.0
        yalign 0.5
        alpha 0.0
        zoom 0.5
        parallel:
            ease_quad 15 zoom 2.0 xalign 0.0
            ease_quad 3 zoom 2.2 alpha 0.0
        parallel:
            ease_cubic 3.0 alpha 1.0
    show rue_2 zorder 1:
        xalign 1.0
        yalign 0.5
        alpha 0.0
        zoom 0.6
        pause 2.0
        parallel:
            ease_quad 15 zoom 2.0 xalign 0.7
            ease_cubic 3 zoom 2.2 alpha 0.0
        parallel:
            ease_cubic 2.0 alpha  1.0
    show set_1 zorder 1:
        xalign 0.8
        yalign 0.9
        alpha 0.0
        zoom 0.8
        pause 3.0
        parallel:
            ease_quad 12 zoom 2.0 xalign 0.6
            ease_cubic 3 zoom 2.2 alpha 0.0
        parallel:
            ease_cubic 6.0 alpha 1.0
    show set_2 zorder 1:
        xalign 0.3
        yalign 0.0
        alpha 0.0
        zoom 0.7
        pause 4.0
        parallel:
            ease_quad 15 zoom 1.6 xalign 0.5
            ease_cubic 3 zoom 2.0 alpha 0.0
        parallel:
            ease_quad 6.0 alpha 1.0
    extend" ha de vestir a la moda lolita,{/i}"

    xh"{font=fonts/AUGUSTUS.ttf}{i}cuyas pruebas ungirán sus genes conurbanenses en la era barroco victoriana...{p}en sus dejos de esperanza de un nuevo renacimiento y nobleza.\"{/i}"
    $ renpy.pause(2.0, hard='True')
    hide rue_1 with Dissolve(0.2)
    hide rue_2 with Dissolve(0.2)
    hide set_1 with Dissolve(0.2)
    hide set_2 with Dissolve(0.2)
    window hide

    hide x onlayer screens with Dissolve(0.5)
    show x closed onlayer screens:
        xalign 0.5
        yalign 0.0
        zoom 2.0
        alpha 0.0
        ease_quad 1.0 zoom 1.8 yalign 0.2 alpha 1.0
        linear 28.0 yalign 0.9
        linear 1.0 alpha 0.0

    $ renpy.pause(2.0, hard='True')

    x"eso sera todo por ahora"

    user"espera un segundo, no me dijiste...cual...es tu nombre????"

    show x smiling onlayer screens
    x"Jajajaja!"
    show x closed onlayer screens
    x"lo mismo me pregunto,{nw}"
    show x seeyousoon onlayer screens with Dissolve(2.0)
    x"{i}jerikito...{/i}"

    python:
        with open("acordate.txt","w") as egg:
            egg.write("acordate " + name + ": \n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nEL TIEMPO NO PASA EN OUTLANDS")
        egg.closed

    play sound "audio/bsss.ogg"
    x"{cps=*0.5}{color=[color_main]}Beshos{/cps}{w=1.0}{nw}"
    $ renpy.pause(4.0, hard='True')
    user"ey un segun{nw}"

    hide x onlayer screens
    hide siteinboxgradient onlayer screens

    hide siteinboxdark
    show screen site_inbox_screen
    show screen db_screen
    stop ambient

    $ renpy.pause(5.0, hard='True')
    x"..."

    hide screen site_inbox_screen with dissolve
    hide screen db_screen
    jump end
