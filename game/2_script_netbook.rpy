define irisoutNetbook = CropMove(0.1, "irisout")

image netbook_desktop:
    "netbook desktop"
    xcenter 0.5
    ycenter 0.5

image netbook_overlay:
    "netbook overlay"
    xcenter 0.5
    ycenter 0.5
    alpha 0.3
    linear .02 alpha 0.35
    linear .02 alpha 0.4
    repeat

screen netbook_screen():
    tag netbook
    zorder 0
    modal False
    frame:
        xalign 0.5
        xfill True
        yfill True
        add "netbook_desktop"

        hbox:
            vbox:
                imagebutton auto "nt_btns/mypc_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_meh')]
                imagebutton auto "nt_btns/bin_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_bin')]
                imagebutton auto "nt_btns/cv1_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_cv')]
                imagebutton auto "nt_btns/cv2_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_cv')]
                imagebutton auto "nt_btns/cv3_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_cv')]
                imagebutton auto "nt_btns/vlc_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_vlc')]
            vbox:
                imagebutton auto "nt_btns/log_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  ShowMenu('net_log')]
                imagebutton auto "nt_btns/ie_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_ie')]
                imagebutton auto "nt_btns/ccleaner_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_meh')]
                imagebutton auto "nt_btns/steam_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_steam')]
                imagebutton auto "nt_btns/dota_%s.png" action [Play("sound", "audio/nt_click_1.mp3"),  Jump('net_dota')]

        add "netbook_overlay"

default quotes=["The weather's good today","I'm bored.","I wanna sleeeeeep !!","Hard-work is really important guys"]

label netbook:
    scene black
    $ renpy.music.set_volume(0.6, 0.6, channel="music")
    play ambient "audio/netbook_noise.mp3"
    $ renpy.pause(4.0, hard='True')

    show screen netbook_screen with irisout
    pause(1.0)

    "Veamos"
    "¿Que estaba por hacer?"

    stop ambient fadeout 9.0
    call screen netbook_screen

    return

# netbook labels
label netbook_display:
    call screen netbook_screen

label net_meh:
    show screen netbook_screen
    "No me hace falta nada aca"
    pause(0.1)
    jump netbook_display

label net_log:
    show screen netbook_screen
    "Este archivo de mierda lo borro y aparece siempre de nuevo"
    pause(0.1)
    jump netbook_display

label net_bin:
    show screen netbook_screen
    "mi vida...{w=0.5}{nw}"
    play sound "audio/linkin.ogg"
    extend"{i}es una papelera de reciclaje{/i}{w=2.0}{nw}"
    stop sound fadeout 0.2
    pause(0.1)
    jump netbook_display

label net_cv:
    show screen netbook_screen
    "Mamá me insiste que busque trabajo,{w=0.2} pero en estos tiempos de incertidumbre,{w=0.2} el tiempo es un capital importantísimo!!!."
    "Igual mi meta es trabajar como ayudante de CEO en Rappi."
    "Con mi experiencia haciendo pedidos,{w=0.2} he recolectado importante información logística y insights."
    pause(0.1)
    jump netbook_display

label net_steam:
    show screen netbook_screen
    "Como futuro youtuber, tengo un estricto régimen de juegos"
    "Simuladores de animales,{w=0.2} juegos en unity de terror,{w=0.2} FNAF...{w=0.5} tengo un pie en cada puerta"
    "La webcam dejo de andar, pero uso un espejo para ver las caras que hago mientras juego"
    "Es importante tener reacciones divertidas"
    ":-D"
    ":-O"
    "estoy haciendo esta cara ahora"
    ":-O"
    pause(0.1)
    jump netbook_display

label net_vlc:
    show screen netbook_screen
    "Me he acostumbrado a ver peliculas de {i}blueray rip 1080p{/i}, aún si se ven lentas."
    "Hoy por hoy,{w=0.2} solo puedo disfrutar una película si tiene blueray."
    "Ayer vi {i}Secreto En la Montaña{/i}...{i}{cps=*2}No es gay!!!{/i}"
    pause(0.1)
    jump netbook_display

label net_dota:
    show screen netbook_screen
    "{cps=*2}Mi sueño es convertirme en el mejor {i}e-gamer{/i} que existe"
    "{cps=*2}Estoy seguro que sólo asi mi padre va a verme por youtube,{w=0.5}\nfestejando mi victoria de la mano de una twitcher de esas que hacen esas caras ahegago :D"
    "{cps=*2}Me queda solo un año más,{w} un año de tomar nota en {i}modo espectador{/i}."
    "{cps=*2}Para ese entonces voy a ser {i}invencible{/i}"
    "{cps=*2}Esos koreanitos no van a saber ni de donde salí. Además{w=0.5}, Tengo una estrategia contra los asiáticos que nadie ha usado hasta ahora:"
    "{cps=*2}Teníamos un compañero en la secundario Raul Nguyen y siempre le pegaban desde arriba. {p}No se daba cuenta nunca."
    "{cps=*2}Atacándolos {i}verticalmente{/}...no van a tener tiempo de verme, puesto que su campo visual es {i}reducido{/i}."
    pause(0.1)
    jump netbook_display

label net_ie:
    show screen netbook_screen
    "Ahora que lo pienso hay algo en mis sueños, aparte de Jerikito..."
    "3...{w=0.1}D...{w=0.1}G?{w=0.1}"
    "Parecía una pagina web"
    "Al menos lo que puedo recordar"
    stop music fadeout 2.0

    pause(0.1)
    hide screen netbook_screen with dissolve
    $ renpy.music.set_volume(1.0, 0.6, channel="music")
    jump site
