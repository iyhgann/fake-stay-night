image fire = Movie(play="decalogue/fire.webm", loop=True)
image fire_whoosh = Movie(play="decalogue/fire_whoosh.webm", loop=False)

image dance_1 = Movie(play="decalogue/dance_1.webm", loop=True)
image dance_2 = Movie(play="decalogue/dance_2.webm", loop=True)
image dance_3 = Movie(play="decalogue/dance_3.webm", loop=True)
image dance_4 = Movie(play="decalogue/dance_4.webm", loop=True)
image dance_5 = Movie(play="decalogue/dance_5.webm", loop=True)

image fade_white = Solid("#FFF")

define flame_pause_time = 2.0

style commandment_style is text:
    # font "fonts/AUGUSTUS.ttf"
    color "#e81505"
    font augustus_font
    text_align 0.5
    size 40

label fire_whoosh:
    window hide
    show fire_whoosh onlayer screens:
        alpha 1.0
        xalign 0.5
        yalign 0.5
        zoom 1.0
        pause 1.0
        linear 1.0 alpha 0.0
    $ renpy.pause(1.5, hard='True')
    return

transform blink:
    linear 1.0 alpha 0.0
    linear 1.0 alpha 1.0
    repeat


image firefly:
    "fire_particle"

image fireflies= SnowBlossom(At("firefly", blink), border=150, count=6000, start=0.00000000001, fast=False,  yspeed=(-100, -80),  xspeed=(-200,200), horizontal=True)


label decalogue:
    scene black with dissolve

    show x remembering onlayer screens
    x"*sigh*"
    $ renpy.pause(2.0, hard='True')
    show x onlayer screens
    x"solo hay una forma de explicarte esto:"
    window hide

    hide x onlayer screens
    show fire_whoosh onlayer screens zorder 2:
        alpha 1.0
        xalign 0.5
        yalign 0.5
        zoom 1.0
        pause 1.0
        linear 1.0 alpha 0.0

    $ renpy.pause(1.5, hard='True')

    play music decalogue_music fadein 2.0
    play ambient "audio/fire_ambience.ogg"

    scene black
    show fire zorder 0:
        alpha 0.0
        xalign 0.5
        yalign 0.5
        zoom 2.0
        linear 2.0 alpha 1.0
    show x fire at x_tf onlayer screens:
        xalign 0.5

    show fireflies onlayer screens

    $ renpy.pause(2.5, hard='True')

    user"{cps=*3}:O"

    show x fire screaming onlayer screens

    xh"{cps=*4}UN FAKE ES UNA DIRECTA VIOLACION {nw}"
    play sound "audio/impact_3.ogg"
    extend"{cps=*4}AL MANDADO CELESTIAL Y LA COMBINACION DE TODOS LOS PECADOS JUNTOS:" with hpunch
    xh"{cps=*4}NECESITAS PRUEBAS??"

    hide x screaming onlayer screens
    call fire_whoosh from _call_fire_whoosh
    image cm1 = Text("Amaras a Dios \n sobre todas las cosas", style="commandment_style", size=30)
    show cm1 zorder 2:
        alpha 0.0
        parallel:
            xalign 0.5 yalign 0.5
            easein 10.0 yalign 0.5 zoom 4
        parallel:
            linear 2.0 alpha 1.0
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0

    $ renpy.pause(flame_pause_time, hard='True')

    play sound "audio/impact_1.ogg"
    xh"{cps=*3.0}UN FAKE SOLO PUEDE AMAR {color=[color_fire]}SWORD ART ONLINE{/color}" with hpunch
    user"hey que te pasa co{nw}"

    hide cm1 with Dissolve(0.4)

    call fire_whoosh from _call_fire_whoosh_1
    image cm2 = Text("No codiciaras \n falsas imagenes", style="commandment_style")
    show cm2 zorder 2:
        alpha 0.0
        parallel:
            xalign 0.5 yalign 0.5
            easein 10.0 yalign 0.5 zoom 4
        parallel:
            linear 2.0 alpha 1.0
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')

    hide fireflies onlayer screens with dissolve

    xh"un fake es pagano iconoclastico por antonomasia, es intrinseco e inherente a su condicion (?)"

    call decalogue_dance_gifs from _call_decalogue_dance_gifs

    xh"{cps=*2}su culto por dibujos adolescentes neotenios{w=0.2} dibujados por japoneses de 45 años con insomnio es prueba de ello"

    hide dance_1 with Dissolve(0.1)
    hide dance_2 with Dissolve(0.1)
    hide dance_3 with Dissolve(0.1)
    hide dance_4 with Dissolve(0.1)
    hide dance_5 with Dissolve(0.1)
    hide dance_6 with Dissolve(0.1)

    hide cm2 with Dissolve(0.1)
    call fire_whoosh from _call_fire_whoosh_2
    image cm3 = Text("No tomarás el\nnombre del Señor\nen vano", style="commandment_style")
    show cm3 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time + 1.0, hard='True')

    show x black onlayer screens:
        alpha 0.0
        xalign 1.0
        yalign 0.3
        zoom 1.2
        parallel:
            ease_quad 1.0 alpha 1.0
        parallel:
            linear 20.0 xalign 0.4
        parallel:
            pause 5.0
            "x black remembering"

    show fireflies onlayer screens

    xh"Cuantos {color=[color_fire]}randoms{/color}, falsos profetas ha visto ir y venir este desdichado forblox??"
    xh"Cuantas {color=[color_fire]}{i}attentionwhores{/i}{/color}??{w=0.5}{cps=*2} Y cuantos hombres se han arrodillado ante éstas blasfemias por una mera foto por PM"
    xh"Por cierto, de dónde crees que surgió Onlyfans?"
    user"{cps=*2.0}TENES UN ONLYF{nw}"

    hide fireflies onlayer screens with dissolve
    hide x onlayer screens with dissolve
    hide cm3 with Dissolve(0.4)
    call fire_whoosh from _call_fire_whoosh_3
    image cm4 = Text("No trabajarás\nel Domingo", style="commandment_style")
    show cm4 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')

    xh"Para no trabajar el domingo, tenes que tener un trabajo en primer lugar."
    play sound "audio/impact_3.ogg"
    xh"{cps=*2}Un fake no ocupa {color=[color_fire]}{i}NINGUNA POSICION DE IMPORTANCIA{/i}{/color} en esta tierra." with hpunch

    hide cm4 with Dissolve(0.4)
    call fire_whoosh from _call_fire_whoosh_4
    image cm5 = Text("Honraras a tu\npadre y a tu madre", style="commandment_style")
    show cm5 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')

    show x black onlayer screens:
        rotate 30
        alpha 0.0
        zoom 1.5
        parallel:
            ease_quad 1.0 zoom 1.0 alpha 1.0
        parallel:
            pause 1.0
            linear 10.0 zoom 1.3

    show fireflies onlayer screens:
        rotate 20
    xh"Tengo que explicar esto??? T_T"
    xh"Y da igual ya que ser fake y huerfano es {i}sinergico{/i}{w=0.1}(?)"

    play sound "audio/impact_2.ogg"
    xh"{font=fonts/AUGUSTUS.ttf}{size=+10}{color=[color_fire]}{cps=*2.0}ACASO TIENES PADRE [name!u]" with hpunch

    user"Bue-bueno mi madre es bastante sexo afectiva,{nw}"
    show x black remembering onlayer screens
    extend"{w=1.0}tengo varios padres de {nw}"

    hide fireflies onlayer screens with dissolve

    hide x black remembering onlayer screens with Dissolve(0.1)
    hide cm5 with Dissolve(0.4)
    call fire_whoosh from _call_fire_whoosh_5
    image cm6 = Text("No matarás", style="commandment_style")
    show cm6 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')
    show x black onlayer screens:
        zoom 2.5
        xpos 400
        ypos -100
        alpha 0.0
        parallel:
            ease_quad 1.0 alpha 1.0
        parallel:
            ease_quad 10.0 xpos 100

    show fireflies onlayer screens

    xh"Una bala puede matarte en la vida real..."
    xh"Un thread fake..."
    play sound "audio/impact_2.ogg"
    extend"{color=[color_fire]}DESTRUYE TU ALMA {w=0.1}(?){/color}"
    xh"{cps=*3}Ni hablar de morir del aburrimiento con los mismos th idiotas una y otra vez."

    hide cm6 with Dissolve(0.4)
    hide fireflies onlayer screens with Dissolve(0.1)
    hide x black remembering onlayer screens with Dissolve(0.1)
    call fire_whoosh from _call_fire_whoosh_6
    image cm7 = Text("No cometeras\nadulterio", style="commandment_style")
    show cm7 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')

    play sound 'audio/world_fapping.ogg' fadein 4.0
    show fireflies onlayer screens
    show x black closed onlayer screens:
        xalign 0.5
        yalign 0.0
        alpha 0.0
        ease_quad 1.0 yalign 0.4 alpha 1.0
        linear 15.0 yalign 0.9
        linear 1.0 alpha 0.0

    x"escuchas?? podes sentir el ruido??"
    x"Entes grasosos tocandose al ritmo de los {color=[color_fire]}BFN{/color},"
    show bfn_1:
        xalign 0.0
        alpha 0.0
        zoom 0.6
        parallel:
            linear 10.0 xalign 0.4
            ease_quad 1.0 zoom 0.7 alpha 0.0
        parallel:
            easeout 1.0 alpha 1.0
    show bfn_2_:
        xalign 0.1
        yalign 1.0
        alpha 0.0
        zoom 0.6
        rotate -10
        pause 1.0
        parallel:
            linear 10.0 xalign 0.1 yalign 1.0 zoom 0.8
            ease_quad 1.0 zoom 0.7 alpha 0.0
        parallel:
            easeout 1.0 alpha 1.0
    show bfn_3:
        xalign 1.0
        yalign 0.9
        alpha 0.0
        zoom 1.2
        pause 1.5
        parallel:
            linear 13.0 xalign 0.8 zoom 1.0
            ease_quad 1.0 zoom 1.1 alpha 0.0
        parallel:
            easeout 1.0 alpha 1.0

    extend"sus cuentas fake permitiendole subir cada vez contenido mas {i}pervertido{/i}"
    user"{cps=*3.0}uuyy mira esos abdominales bb!!!"
    call like_perro_label from _call_like_perro_label

    stop sound fadeout 1.0

    hide bfn_1 with Dissolve(0.1)
    hide bfn_2_ with Dissolve(0.1)
    hide bfn_3 with Dissolve(0.1)
    hide x onlayer screens with dissolve
    hide fireflies onlayer screens with dissolve
    hide cm7 with Dissolve(0.4)
    call fire_whoosh from _call_fire_whoosh_7
    image cm8 = Text("No robaras", style="commandment_style")
    show cm8 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')

    xh"un fake es un ladron de identidad,{p}Que hay de las horas robadas, por lo incosecuencial de sus threads"

    hide cm8 with Dissolve(0.4)
    call fire_whoosh from _call_fire_whoosh_8
    image cm9 = Text("No daras\nfalso testimonio\ncontra tu\n projimo", style="commandment_style")
    show cm9 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')

    xh"ohhh pero espera a crearte otra cuenta usando tu mail alternativo.{p}Entonces lo haras. {w=0.5}{nw}"
    play sound "audio/impact_3.ogg"
    extend"{color=[color_fire]}{cps=*3}DARAS FALSO TESTIMONIO Y DISFRUTARAS CADA MOMENTO."

    hide cm9 with Dissolve(0.4)
    call fire_whoosh from _call_fire_whoosh_9
    image cm10 = Text("No codiciaras\nla mujer de\n tu projimox", style="commandment_style")
    show cm10 zorder 2:
        parallel:
            alpha 0.0 xalign 0.5 yalign 0.5
            linear 10.0 alpha 1.0 yalign 0.5 zoom 5
        parallel:
            pause 8.0
            linear 2.0 alpha 0.0
    $ renpy.pause(flame_pause_time, hard='True')

    xh"Solo la mujer del projimo?{p}que hay de su {color=[color_fire]}hermana{/color}?"
    play sound "audio/impact_4.ogg"
    xh"{cps=*3}QUE HAY DE CODICIAR AL {color=[color_fire]}{i}PROJIMO MISMO{/i}{/color} CUANDO TE SALE EN TINDER O EN SU DEFECTO BADOO CON UNA PELUCA" with hpunch

    $ renpy.pause(1.0, hard='True')
    xh"{i}{w=0.1}.{w=0.1}.{w=0.1}.{w=0.5}[name], yo...{/i}"

    hide cm10
    hide fire_whoosh onlayer screens
    stop music fadeout 2.0
    $ renpy.pause(1.0, hard='True')
    window hide
    scene black with Dissolve(1.0)
    stop ambient fadeout 4.0
    $ renpy.pause(6.0, hard='True')

    jump hope
