# The game starts here.
style disclaimer_text_style is image:
    size 40
    color "#f1921a"
    yalign 0.5
    text_align 0.5
    xsize 900

image disclaimer_text = Text("Los hechos y personajes en esta historia son falsos. \nToda semejanza con la realidad es pura coincidencia.", style="disclaimer_text_style")

label start:
    stop music fadeout 2.0
    show disclaimer_text with dissolve
    $ renpy.pause(2.0, hard='True')
    jump start_game

label start_game:
    scene black with dissolve
    pause 2.0

    window hide

    play ambient 'audio/nightmare.ogg' fadein 2.0
    play sound [ "<silence 1.5>", 'audio/jerikito.ogg']
    $ renpy.pause(5.0, hard='True')

    centered "Pero...{w=0.5}que esta pasando..."

    centered "Otro de esos sueños otra vez..."

    play music 'audio/berserk_monster.ogg' fadein 4.0

    centered "Que puede estar sucediendo"

    play sound 'audio/godhand_licha.ogg' fadein 2.0
    show gh_licha:
        parallel:
            alpha 0.0 xalign 0.5 zoom 1.0 yoffset 500
            linear 6.0 alpha 1.0 xalign 0.5 yoffset 50 zoom 2.0
            easein 2.0 alpha 0.0 zoom 2.2
        parallel:
            Color("#000")
            pause .02
            "god_hand/licha.png"
            pause .06
            repeat

    $ renpy.pause(2.0, hard='True')
    top"{cps=*2}{color=[color_main]}{size=+5}AHH OTRA VEZ ESTO!!!{w=2.0} NO ESTOY INTENTANDO MATARTE!!" with hpunch
    $ renpy.pause()
    # $ ui.text("{cps=*2}No estoy intentando matarte!!!", slow=True, xalign=0.5, ypos=100)
    # $ renpy.pause()

    centered "{cps=*2}Es el {i}gobierno de Suecia {/i}, o que se yo!!"

    stop sound fadeout 1.0
    hide gh_licha with Dissolve(1.0)
    window hide

    centered "{cps=*2}Maldición{w=0.5}{cps=*3} tengo que mantener la calma!!"

    play sound 'audio/godhand_sinon.ogg'
    show gh_sinon:
        parallel:
            rotate 45.0 alpha 0.0 xoffset -150.0 zoom 1.0 yoffset 100
            linear 6.0 alpha 1.0 xoffset -250.0 yoffset -300 zoom 2.0
            linear 2.0 alpha 0.0 zoom 2.2
        parallel:
            Color("#000")
            pause .02
            "god_hand/sinon.png"
            pause .06
            repeat

    $ renpy.pause(2.0, hard='True')
    right"{size=+15}{color=[color_main]}AHHH!!!" with vpunch

    right"{size=+5}{color=[color_main]}{cps=*2}PERTURBADOR!!!"
    $ renpy.pause()

    centered "{cps=*2}Es lo mismo una y otra vez..."
    centered "{cps=*2}Cómo escapo de esto???"

    stop sound fadeout 1.0
    hide gh_sinon with Dissolve(1.0)
    window hide

    play sound [ "<silence 1.5>", 'audio/godhand_dia.ogg']
    show gh_dia:
        parallel:
            rotate -45.0 alpha 0.0 xalign 1.5 zoom 1.0 yoffset 0
            linear 6.0 alpha 1.0 xalign 1.5 yoffset -200 zoom 2.0
            linear 2.0 alpha 0.0 zoom 2.2
        parallel:
            Color("#000")
            pause .02
            "god_hand/dia.png"
            pause .06
            repeat

    $ renpy.pause(1.0, hard='True')
    centered "{cps=*2}Oh no..."
    centered "{cps=*2}Eran mis favoritas..."
    centered "{cps=*2}Por lo menos, antes de que sucediera todo ésto"

    stop sound fadeout 1.0
    hide gh_dia with Dissolve(0.5)
    window hide

    $ renpy.music.set_volume(0.2, 0.6, channel="music")
    play sound 'audio/godhand_rue.ogg'
    show gh_ahiru:
        parallel:
            rotate 225.0 alpha 0.0 xalign 1.3 yoffset -150 zoom 1.0
            linear 6.0 alpha 1.0 xoffset 200.0 yoffset -500.0 zoom 2.0
            easein 2.0 alpha 0.0 zoom 2.2
        parallel:
            Color("#000")
            pause .02
            "god_hand/ahiru.png"
            pause .06
            repeat

    $ renpy.pause(1.0, hard='True')
    left"{size=+15}{cps=*2}{color=[color_main]}AHHHGHHHH!!!" with vpunch
    $ renpy.pause(2.0, hard='True')
    bottom_right"El {i}chico del ballet{/i} de nuevo!!!"
    centered "Es que acaso...no hay ropa de ballet {i}para hombres{/i}???"

    stop sound fadeout 1.0
    hide gh_ahiru with Dissolve(0.5)
    $ renpy.music.set_volume(1.0, 0.6, channel="music")
    window hide

    centered "NO PUEDO SOPORTARLO"
    centered "No puedo seguir con esto"
    centered "y lo peor de todo..."
    centered "Está {i}viéndome{/i}"
    centered "{size=+5}{cps=*5}{i}Me estas escuchando acaso???"
    centered "{size=+10}{cps=*5}{i}Estoy harto de ésto!!" with hpunch
    centered "{size=+10}{cps=*0.5}{i}{color=[color_main]}Donde estás???!!"

    show gh_jeri:
        parallel:
            alpha 0.0 xalign 0.5 yoffset 300 zoom 1.0
            linear 20.0 alpha 1.0 yoffset -310 zoom 3.0
        parallel:
            Color("#000")
            pause .02
            "god_hand/jeri.png"
            pause .06
            repeat

    play sound "audio/impact_3.ogg"
    pause 1.0
    top"{size=+10}{cps=*2}OH NO" with hpunch
    top"{size=+10}{cps=*2}NO NO NO NO"
    centered "{size=+40}{cps=6}{color=[color_main]}AHHHHHHHHHHHHHHHHH!!!" with flash

    stop music fadeout 2.0
    scene black with Dissolve(2.0)
    stop ambient fadeout 4.0
    $ renpy.pause(5.0, hard='True')

    centered "En ese momento no lo sabía pero{cps=*0.2}...{/cps}"
    centered "...mi vida..."
    centered "{w=0.5}estaba por {i}cambiar{/i}..."

    play ambient 'audio/street_scene.mp3' fadein 4.0

    pause(1.0)

    "Ahh..."

    "...pero que sueño...{p}{w=0.5}...tan extraño{w=0.2}.{w=0.2}.{w=0.2}."

    "Todas las mañ-{nw}"

    play sound 'audio/door_mom.ogg'

    centered "{i}{cps=100} DALE BOLUDO LEVANTATE QUE SON LAS DOS Y MEDIA YA!!" with hpunch

    play sound 'audio/making_bed.mp3'

    "ehh ahh"

    $renpy.pause(1.0, hard='True')

    scene room bed with dissolve
    $renpy.pause(1.0, hard='True')

    "Ahh...listo"

    play music 'audio/Sahara_Sky_-_07_-_Sun_Goes_Down_acoustic.mp3' fadein 2.0

    "Debería no esforzarme tanto, ya tendí la cama más tarde saco la basura y con eso concluiría la actividad del día"

    "Desde que mi padre desapareció misteriosamente{w=0.2} unos instantes después de eyacular adentro de mi madre,{w=1.0} las cosas han estado complicadas en mi {i}clan{/i}{w=0.5}(?)."

    "Pero no debo dejar que me afecte."

    stop ambient fadeout 2.0
    scene room desk with dissolve
    $renpy.pause(1.0, hard='True')

    "Ah la fiel netbook del gobierno..."

    "Con un procesador Intel® Atom N455, display LCD 10,1\"\" y un 1GB DDR3 no tengo nada que {i}nVidiarle{/i} a nadie."

    "...{w=0.5}{cps=*2}3 puertos USB 2.0!!!"

    "Digan lo que quieran de los gobiernos socialistas, pero... {w=0.5}{p}¡éstas netbooks se zarpan!!!"

    "Tira el Dota2 a unos asombrosos {i}15fps{/i}, lo cual me da tiempo para calcular mis acciones. {p}Se podría decir que soy un {i}thinker{/i}{w=0.5}(?)"

    scene black with dissolve

    jump netbook

    # This ends the game.
    return
