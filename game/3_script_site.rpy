#screen vars
define browsing_clicks = 0

label site:
    scene black
    show screen site_screen(isLogged, browsing_clicks) with pixellate
    show screen db_screen # overlay for clicking in site_screen
    play music site_music
    $ renpy.pause(1.0, hard='True')

    "Ahhhhhh"

    "Qué extraño{w=0.5}.{w=0.5}.{w=0.5}.es mucho texto"

    "La gente sigue escribiendo cosas todavía???"

    "Escribir...{w=1.0}{cps=*4.0}{i}Me gusta!!!"

    "Ok supongo debería crear una cuenta"
    $ renpy.music.set_volume(0.4, 0.6, channel="music")
    $can_log = True

    hide screen db_screen
    hide screen site_screen with dissolve
    scene black
    call screen site_screen(isLogged, browsing_clicks, can_log) with dissolve

label site_register:
    $can_log = False
    show screen site_screen(isLogged, browsing_clicks, can_log)
    show screen db_screen

    "{cps=*2.0}Ay para la puta que te re pario!!!{/cps}" with hpunch
    who"Heyyy"

    $ renpy.music.set_volume(0.2, 0.2, channel="music")
    play sound "audio/tense_breathing.ogg"
    show 3dgchan oligofrenic at chan_tf onlayer screens:
        rotate -5
        xalign 3.2
        linear 40 xalign 0.5

    "???"

    hide screen db_screen
    $ renpy.pause(8.0, hard='True')
    show screen db_screen
    "..."
    "-_- {i}crinye{/i}..."
    ">_< no en serio..."
    "no estoy entendien{nw}"

    stop sound
    play sound "audio/appear_1.ogg"
    hide 3dgchan oligofrenic
    show 3dgchan at chan_tf onlayer screens:
        rotate 0
        xalign 0.5
        yalign 0.5
    show screen site_screen(isLogged, browsing_clicks) with flash

    pause 1.0
    show 3dgchan greet at chan_tf onlayer screens:
    who"Como éstas infeliz!{w} Mi nombre es {size=+10}{color=[color_main]}3DG CHAN{/color}"

    show 3dgchan at chan_tf onlayer screens

    c"Bievenido al rincón de internet donde la magia a duras penas sucede!"
    c"¡No paramos de ganar!"
    show 3dgchan smiling at chan_tf onlayer screens
    extend" Todos los días es una fiesta."
    c"Justo ahora estamos refaccionando, un par de retoques allí y allá."

    show 3dgchan at chan_tf onlayer screens

    c"¿Pero antes de participar, qué tal si nos das algunos datos?"
    c"Danos tu nombre para empezar {cps=*3}(con la tecla {color=#f1921a}Enter{/color})"

    # play sound "audio/choose.ogg"
    python:
        name = renpy.input("Nombre:")
        if not isinstance(name, (bool)):
            name = name.strip()
        else:
            renpy.show("3dgchan bothered", [], "screens")
            renpy.say(c, "{cps=*3}Apretame {size=+10}{color=[color_main]}Enter{/color}{/size} que se me rompe el pendorcho{/cps}")
            name = renpy.input("Nombre:")

            if not isinstance(name, (bool)):
                name = name.strip()
            else:
                name = "blee"
        # name = name.strip() or "coolio"

    define user = Character("{color=#f1921a}[name]{/color}")

    show 3dgchan at chan_tf onlayer screens

    user"Me llamo [name] :D ^-^"

    c"Ok [name], podrías indicarnos tu color de piel?"

    show 3dgchan onlayer screens:
        xalign 0.5
        linear 0.5 xalign 1.8

    pause(1.0)

    call screen skin_screen with dissolve

    show 3dgchan onlayer screens:
        xalign 0.5

    c"¡Muy bien!"

    menu:
        "¿Sos hombre o mujer?"

        "Hombre":
            $ sex = SEX_MALE

        "Mujer :D":
            $ sex = SEX_FEMALE

    show 3dgchan smiling at chan_tf onlayer screens
    c"Sos {i}hombre{/i}."

    show 3dgchan at chan_tf onlayer screens
    c"Bueno [name]{w=0.5}{nw}"
    show 3dgchan laughing at chan_tf onlayer screens
    extend"{i}-jaja, [name]-{/i}{w=0.5}{nw}"
    show 3dgchan at chan_tf onlayer screens
    extend"espero que te diviertas en nuestra comunidad!"
    c"Por cierto...{w=0.5}querés...{w=0.5}{nw}"
    show 3dgchan bothered at chan_tf onlayer screens
    extend"{cps=*0.2}{i}sentirte bien™{/i}?"
    user"¿Cómo?{w=0.5} Ehh{w=0.5} Sí porq{nw}"
    show 3dgchan presenting smiling onlayer screens
    c"{w=0.5}¡Es ahora tu oportunidad de donar!" with hpunch
    show 3dgchan smiling onlayer screens:
        rotate 0
        easein 0.5 rotate 5
    c"Te damos un {i}Badge de Honor™{/i}!{p}{nw}"
    show 3dgchan smiling onlayer screens:
        rotate 5
        easein 0.5 rotate -5
    extend"Acceso al {i}Foro Privado™{/i},{p}{nw}"
    show 3dgchan onlayer screens:
        rotate -5
        easein 0.5 rotate 0
    extend"y adivina que:"
    c"No vas a tener que ver la publicidad que ya ocultas con tu {nw}"
    show 3dgchan bothered at chan_tf onlayer screens
    extend"{size=+10}{cps=*0.1}{i}MUGROSO{/i}{/cps}{/size}{nw}"
    show 3dgchan at chan_tf onlayer screens
    extend"adblocker!"


    menu:
        "¿Qué decis?"

        "Me encantaría":
            jump donar

        "Ni hablar, es internet, I'm a {i}pirate™{/i} baby":
            jump not_donar

label donar:
    show 3dgchan moved at chan_tf onlayer screens
    c"...{w=0.5}¿En serio?"
    c"Sabía...{w=0.5}{cps=0}*snif*{cps=75}{w=0.5}que podia contar{w=0.5}{cps=0}*snif*{cps=75}{w=0.5} contigo!"
    c"{cps=40}Podemos comenzar, solo tenes que pasarme tu numero de tarjeta de cred{nw}"

    show 3dgchan laughing hair at chan_tf onlayer screens with hpunch
    c"{fast}AJAJJSSSSSJAAJSJSJJAJAJSja"

    show 3dgchan smiling at chan_tf onlayer screens
    c"jaja ahhh{w=0.5} Deja pibe, andá nomas."
    jump have_fun

label not_donar:
    show 3dgchan smiling onlayer screens
    c"¿Por qué pense por un momento que podias refractar {i}determinados espectros{/i} de la luz con tu piel y a la vez ser agradecido?"
    jump have_fun

label have_fun:
    show 3dgchan at chan_tf onlayer screens
    c"No dudes en llamarme por cualquier cosa! Divertite!"

    $ isLogged = True

    window hide
    show 3dgchan oligofrenic at chan_tf onlayer screens
    hide 3dgchan oligofrenic onlayer screens with Dissolve(5.0)
    hide screen db_screen
    hide screen site_screen with dissolve
    scene black
    call screen site_screen(isLogged, browsing_clicks) with dissolve

    return
