init -1 python:
    def play_effect(trans, st, at):
        renpy.play("audio/horse_2.ogg", channel="sound")

init python:
    saturation = 1.0

    def bwImage(st,at,path):
        return im.MatrixColor(Image(path),im.matrix.saturation(saturation)),None

image site_outlands_posted = DynamicDisplayable(bwImage, "bg/siteoutlandsposted.png")
image site_nav_logged_outlands = DynamicDisplayable(bwImage, "bg/sitenavlogged.png")

style site_button_outlands is button:
    xsize 100
    ysize 100
    spacing 10

style site_post_label is text:
    size 12
    kerning -1

image replymsg_button:
    "replymsg_idle"
    zoom 0.675

image replypost_button:
    "replypost_idle"
    zoom 0.675

screen site_outlands_screen(hasReplies=False, hasMessages=False, hasPosted=False):
    tag site
    zorder 0
    modal False

    side "c r":
        viewport id "vp_outlands":
            mousewheel True
            arrowkeys True

            frame:
                background Solid("#24262c")
                xfill True

                vbox:
                    xalign 0.5

                    showif not hasPosted:
                        add "bg/siteoutlands.png":
                            zoom 0.60
                            xalign 0.5
                            ycenter 0.5
                        textbutton _("Crear Tema"):
                            style "site_button"
                            xoffset 80
                            yoffset -2412
                            action [Play("sound", "audio/nt_click_1.mp3"),  Jump('site_outlands_writing')]
                    else:
                        add "site_outlands_posted":
                            zoom 0.60
                            xalign 0.5
                            ycenter 0.5
                        text "[name], 25-02-05 11:26 PM":
                            style "site_post_label"
                            yoffset -2052
                            xoffset 132
                        text "[postRepliesNumber]":
                            style "site_post_label"
                            yoffset -2082
                            xoffset 728
                        text "[postViewsNumber]":
                            style "site_post_label"
                            yoffset -2099
                            xoffset 810
                        text "[name]":
                            style "site_post_label"
                            color "#ffb84e"
                            yoffset -2120
                            xoffset 872

        vbar value YScrollValue("vp_outlands"):
            unscrollable "hide"

    fixed:
        add "site_nav_logged_outlands":
            zoom 0.675
            xcenter 0.5
            ypos -0.005
        showif hasReplies:
            imagebutton:
                idle "replypost_button"
                action [Play("sound", "audio/nt_click_1.mp3"),  Jump('site_outlands_replies_read')]
                xpos 1090
                yoffset -3
        showif hasMessages:
            imagebutton:
                idle "replymsg_button"
                xpos 1130
                yoffset -3
                action [Play("sound", "audio/nt_click_1.mp3"),  Jump('site_outlands_go_inbox')]

label site_outlands:
    scene black
    stop music fadeout 2.0
    show screen site_outlands_screen with pixellate
    show screen db_screen # overlay for clicking in site_screen

    pause 1.0

    show 3dgchan at chan_tf_subs onlayer screens with pixellate
    user"ohhhh,{w} {i}~sugooooooooooooiii~{/i}{nw}"
    show 3dgchan bothered hair at chan_tf_subs onlayer screens
    extend"{w=2.0} asi que esto {nw}"
    show 3dgchan at chan_tf_subs onlayer screens
    extend "es outlands"

    c"esto es Outlands sí, por lo menos lo queda"

    show 3dgchan presenting smiling at chan_tf_subs onlayer screens
    c"hemos tenido un nuevo influjo de personas sorprendentemente"
    c"se ha vuelto un poco mas lgbt fr-{w=0.1}.{w=0.1}.{w=0.1}."

    show 3dgchan smiling at chan_tf_subs onlayer screens
    c"solo dejemoslo en que se volvio un lugar más {i}PUTO*{/i}.\n\n{cps=*10}{size=-13}(*)Las opiniones presentadas por personajes de este juego no necesariamente reflejan las opiniones de su autor, y aún así si las representase, no serían expresadas de está manera en vista de estar en un lugar público, a menos que fuera victima de un ataque impromptu que acepta una defensa verbal y la persona muestre caracteristicas que permitan usar el pejorativo, como por ejemplo tener un iphone y ser de clase media o ser fan de videos de freestyle.{nw}"

    c"Bueno, yo creo que de acá te vas a poder manejar solo"
    show 3dgchan at chan_tf_subs onlayer screens
    c"Cualquier cosa que necesites, apreta el botoncito como una {color=[color_main]}x{/color} en la esquina superior de tu pantalla"
    c"Chauuu {nw}"

    show 3dgchan onlayer screens:
        alpha 1.0
        ease_quad 1.5 alpha 0.0

    user"Mmmh...{w=0.5}que debería hacer???"

    hide screen db_screen
    call screen site_outlands_screen
    return

label site_outlands_writing:
    show screen site_outlands_screen
    show screen db_screen

    nvl clear
    user"Por supuesto! claramente tengo que crear un {i}tema{i}" with hpunch
    user"{i}okay{/i} veaaaamos,{w=0.5} que puedo escribir?"
    user"Es importante tener {i}buena dicción{/i},{w} al menos eso decía mi {i}ex-padrastro{/i}"
    user"{i}La mirada alta, las mangas abotonadas y una firme apretada de manos!{/i}{p}Re buen vendedor,{w=0.4} pensar que mamá sigue pagando el set de ollas."
    user"ok, manos a la obra"
    play music posting_music fadein 2.0

    $ gui.nvl_text_xpos = 450
    $ gui.nvl_text_ypos = 50
    $ gui.nvl_text_width = 590
    $ gui.nvl_text_xalign = 0.0

    pause 1.0

    nvl clear
    $ post = "Ola chikos que aSeN,"
    userpost"[post]"

    user"Excelente, un comiezo cortés, {i}clásico{/i}"
    user"Continuemos"

    menu:
        "{cps=0}cuál es el drama?"

        "hay alguna chica en este foro":
            $ postLine = "ai alguna khica en este foro que le kbe un macho riko osi escribanme a {0}@outlook.com".format(name)
        "me gusta una chica":
            $ postLine = "les kmento tengo un problema me gusta una chica que esta guay, yo ke ando ciempre labeo y es como hoyekeriko"
        "quiero ver el video porno del que tanto hablan":
            $ choosed_porn = True
            $ postLine = "kiciera saber quien tiene el video de la khica del tiempo alguien puede subirlo a youtube tengo el control parental en mi netbook no se kmo sakrlo"

    pause 0.5
    nvl clear
    userpost"{cps=0}[post]{cps=40} [postLine]"
    $ post += postLine

    user"un buen comienzo...ahora,"

    menu:
        user"necesito llamar la atencion"
        "voxed":
            $ choosed_voxed = True
            $ postLine = "YO LA VERDAD PREFIERO VOXED OSEA, TIENE MAS CONTENIDO, COMO IMAGENES Y PUEDO ESCROLEAR SIN FINAL Y SEGUIR VINDO KSAS :D"
        "exhortar el travestismo":
            $ choosed_trava = True
            $ postLine = "LA VERDAD NUNKA ME COJI UN TRAVA PERO SI NINJA O VEGETA777 (SOY YOUTUBER OLDSKOOL ;-)) FUERAN KMO TRAVAS PORQUE NO TENER UNA NOCHE DE PACION"
        "hablar como chileno sin razon alguna":
            $ choosed_chilean = True
            $ postLine = "WEONNN ES LA NETA GUEI, XDD, KE POS KMO SOI CHILENO ASIPOS UEONN"

    pause 0.5
    nvl clear
    userpost"{cps=0}[post]{cps=40} [postLine]"
    $ post += postLine

    user"{i}\"La letra mayúscula infiere accion y asertividad\"{/i}"

    user"debería agregar signos de exclamación"

    menu:
        "{cps=0}Cuántos \"!\" agrego"

        "1":
            $ postLine = "!"
        "3":
            $ postLine = "!!!"
        "142":
            $ choosed_max_exclamations = True
            $ postLine = 142 * "!"

    nvl clear
    userpost"{cps=0}[post]{cps=40}[postLine]"
    $ post += postLine

    user"Otra cosa que me molesta.{w} El {i}contraste{/i}.{w} ¡me está {i}matando{/i}!"
    user"¿Quién pone letra {i}clara{/i} contra fondo {i}oscuro{/i}?{p}Claramente son {u}dos colores diferentes.{/u}"
    user"A ver"

    pause 0.2
    play sound 'audio/nt_click_1.mp3'

    nvl clear
    userpost"{color=#32373f}{cps=0}[post]{cps=40}{/color}"

    user"Ahhhhh mucho mejor"
    user"Ok Hagamoslo!"

    play sound 'audio/nt_click_1.mp3'
    pause 1.0

    window hide
    play sound 'audio/send_post_1.ogg'
    stop music fadeout 2.0

    hide screen site_outlands_screen
    hide screen db_screen
    scene white with dissolve
    $ renpy.pause(5.0, hard='True')

    $ posted = True

    jump site_outlands_posted

label site_outlands_posted:
    show screen site_outlands_screen(replies, messages, posted)
    show screen db_screen
    pause 2.0

    user "Oook"
    user "Voy a irme a descansar y despues vemos que{nw}"
    play sound 'audio/notification.ogg'

    $ replies = True
    show screen site_outlands_screen(replies, messages, posted)
    window hide

    pause 2.0

    user "eeh"
    user "tan rapido???"

    hide screen db_screen
    call screen site_outlands_screen(replies, messages, posted) with dissolve

label site_outlands_replies_read:
    $ replies = False
    show screen site_outlands_screen(replies, messages, posted)
    show screen db_screen

    user"Bueno, hora de ver que me dicen!!"
    user"Tengo la impresión de que les voy a caer re bien!"
    user"Usé toda mi experiencia de años {color=[color_main]}friki{/color} :P soy basicamente el protagonista de una serie"
    user"A veces no me dejo de sorprender lo mucho que se asemeja el anime con la realidad"
    user"Bueno a ver:"

    pause 2.0

    azure"anda al gym jerikito"
    play music reading_posts_music fadein 5.0
    user"ehp"

    $ saturation = 0.95

    sidon"pelotudo"
    user"...{w=0.5}no entiendo..."
    $ saturation = 0.90
    call like_perro_label from _call_like_perro_label_1
    cacop"pelotudo"

    $ saturation = 0.80

    emrz"Muy interesante, Jerikito.\n{cps=*3}Bottom Text.{/cps}"

    $ saturation = 0.70

    helpmehelpyou"{cps=0}tuve{/cps}{w=0.2} {cps=0}que{/cps}{w=0.5}{cps=0}pagar{/cps}{w=0.2} {cps=0}otro{/cps}{w=0.2} {cps=0}cafe{/cps}{w=0.2} {cps=0}en{/cps}{w=0.2} {cps=0}el{/cps}{w=0.2} {cps=0}bar{/cps}{w=0.2} {cps=0}para{/cps}{w=0.2} {cps=0}esto??{/cps}{w=2.0} +10"

    if choosed_porn:
        oda"jaja. link del video por MP."

    rouge"Patético jerikito."

    user"{cps=*3}ESTO NO ERA LO QUE ESPERABA!!!"

    play sound "audio/magic_wand.mp3"
    queue sound "audio/horse_1.ogg"
    show spony onlayer screens:
        zoom 0.8
        xalign 2.5
        yalign 0.5
        alpha 0.0
        parallel:
            ease_quad 2.0 xalign 1.2 alpha 1.0
        parallel:
            ease_quad 1.0 rotate 16.0
            easein 0.2 rotate 0.0
            pause 1.0
            ease_quad 0.3 rotate 12.0
            easein 0.2 rotate 0.0
            pause 3.0
            ease_quad 0.3 rotate 5.0
            easein 0.2 rotate 0.0
            repeat
        parallel:
            pause 10.0
            function play_effect
            ease_quad 1.0 rotate 16.0
            ease_bounce 2.0 xalign -2.0

    spon"juejuejue jerikito no cambias mas boludo"

    user"ahghhh que..."

    kom"Me cuesta creer todavía que el viejo se hizo fana de los ponies."

    if choosed_voxed:
        deedeemuss"Voxed."
        user"no...les gusta voxed???!"

    $ saturation = 0.50

    if choosed_max_exclamations:
        call like_perro_label from _call_like_perro_label_2
        edroh"Mira la cantidad de signos de exclamacion que pusiste pelotudo"

    $ saturation = 0.0

    ibrn"{cps=*2}Chicos muy bueno todo la verdad, sigan así esto va para mejor."
    hide spony onlayer screens

    user"Esta utilizando {i}sarcasmo{/i} :-( ???...{w=0.1}no,{w=0.2} no puede ser"
    user"Tiene que ser sarcasmo."

    edroh"Hijo de puta"
    user"yo..."

    bluesky"Jerikito y la concha bien de tu madre"
    user"yo..."

    if choosed_trava:
        ken2"eres de los míos jerikito debo decirlo"

    robbie"sos terrible corky jerikito"

    user"Tengo que salir de acá!!!" with hpunch

    window hide
    scene black with dissolve
    hide screen site_outlands_screen with dissolve
    hide screen db_screen

    stop music fadeout 2.0
    $ saturation = 1.0
    $ renpy.pause(2.0, hard='True')
    show screen site_outlands_screen(replies, messages, posted) with dissolve
    show screen db_screen

    user"Qué{w=0.5}.{w=0.5}.{w=0.5}.{w}qué ha sido eso"
    user"No solo no respondieron mis dudas...{w=1.0}siguen llamandome{w=0.1}.{w=0.1}.{w=0.1}."

    if choosed_chilean:
        user"mis posts chilenos siempre eran sensacion en reddit, nunca recibi gold pero{nw}"

    user"y lo peor de todo es que...{w=1.0}sigo sin entender nada!!!"

    $ messages = True
    window hide
    play sound 'audio/notification.ogg'
    show screen site_outlands_screen(replies, messages, posted)
    $ renpy.pause(1.0, hard='True')
    user"???"
    hide screen site_outlands_screen
    hide screen db_screen
    call screen site_outlands_screen(replies, messages, posted)

label site_outlands_go_inbox:
    jump site_inbox

label like_perro_label:
    show like_perro onlayer screens:
        xalign .4 yalign .7 alpha 1 subpixel True
        parallel:
            linear 0.8 yalign .65
        parallel:
            linear 0.8 alpha 0

    with dissolve
    return
