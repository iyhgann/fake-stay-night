label mario_click:
    show screen site_screen
    show screen db_screen
    show mario at truecenter onlayer screens

    $rand_dialogue = renpy.random.randint(1, 5)

    if rand_dialogue == 1:
        m"Leyendo el foro de sexualidad me estoy dando cuenta de mis actitudes beta frente a la princesa"
        m"O sea, elKiobe tiene razón, me estoy dejando boludear"
    elif rand_dialogue == 2:
        m"La cantidad de pelotudeces que he visto hermano..."
    elif rand_dialogue == 3:
        m"Hubo un tiempo que me cojía una pendeja putona en world 1-3"
        m"Tenia un fetiche con el traje tanuki, no era muy linda de cara"
        m"Igual la tenía con el ex: que me toca y me mata, que solo tengo una vida, no puedo continuar, yo lo pasé por arriba al calabaza."
        m"Pensar que soy un plomero, loco. Anecdotas así tenemos todos"
    elif rand_dialogue == 4:
        m"Soy muy petiso para badoo?"
    elif rand_dialogue == 5:
        m"Para mi hermano me esta cagando, loco que queres que te diga"

    hide mario onlayer screens
    jump site_label_hide

label decalogue_dance_gifs:
    show dance_3:
        xalign 0.8
        yalign 0.8
        alpha 0.0
        zoom 2.0
        parallel:
            linear 7 xalign 0.4
            linear 2.0 alpha 0.0
        parallel:
            linear 0.5 alpha 1.0
    show dance_1:
        xalign 0.0
        yalign 0.3
        alpha 0.0
        rotate 10
        zoom 0.5
        parallel:
            linear 8 zoom 0.7 xalign 0.2
            linear 0.4 alpha 0.0
        parallel:
            linear 0.5 alpha 1.0
    show dance_2:
        xalign 0.8
        yalign 0.5
        alpha 0.0
        zoom 1.0
        parallel:
            linear 7 xalign 0.5
            linear 2.0 alpha 0.0
        parallel:
            linear 0.5 alpha 1.0
    show dance_4:
        xalign 0.8
        yalign 0.0
        alpha 0.0
        zoom 1.0
        pause 1.0
        parallel:
            linear 7 xalign 0.9 yalign 0.7
            linear 2.0 alpha 0.0
        parallel:
            linear 0.5 alpha 1.0
    show dance_5:
        xalign 0.0
        yalign 1.0
        alpha 0.0
        zoom 1.0
        pause 2.0
        parallel:
            linear 7 xalign 0.2 yalign 0.7
            linear 2.0 alpha 0.0
        parallel:
            linear 0.5 alpha 1.0
    return
