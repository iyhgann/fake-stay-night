label site_label_hide:
    hide screen db_screen
    hide screen site_screen with Dissolve(0.1)
    scene black
    call screen site_screen(isLogged, browsing_clicks) with Dissolve(0.1)

label site_outlands_time:
    if  browsing_clicks >= 2 and browsing_clicks <= 5:
        show 3dgchan at chan_tf_subs onlayer screens
        c"A donde mierda estas intenando entrar"
        c"Quieras donde quieras, vas a terminar en el mismo lugar..."
    elif browsing_clicks >= 5:
        show 3dgchan bothered hair at chan_tf_subs onlayer screens
        c"{cps=*0.5}Tenes que hacer click en outlands.{/cps}"
    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_outlands:
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen

    user"{cps=10}Out{cps=60}{w=0.5}lands???"
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)
    c"yess"
    c"Me olvide de decirte, pero sí.{w} Vos te quedás acá."
    c"Sos más de la onda,"
    show 3dgchan bothered at chan_tf_subs onlayer screens
    extend"mmmm...como decirlo"
    show 3dgchan at chan_tf_subs onlayer screens
    c"{i}Irremediablemente fracasado.{/i}"
    show 3dgchan presenting smiling at chan_tf_subs onlayer screens
    c"Lindo corral nos mandamos quedate tranquilo tenés todo cubierto."

    user"Mi sueño es ser youtuber."
    show 3dgchan smiling at chan_tf_subs onlayer screens
    c"Por supuesto...sí."

    show 3dgchan disgusted at chan_tf_subs onlayer screens
    hide 3dgchan disgusted onlayer screens with dissolve
    hide screen site_screen with dissolve
    hide screen db_screen
    jump site_outlands

label site_label_bienvenidas:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen

    user"Yeeee,{w=0.5} ya era hora de que me conozcan"

    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"claro, por que es tu primera vez acá"
    user"Como? si, por que?"
    user"..."
    show 3dgchan at chan_tf_subs onlayer screens
    c"ehhh"

    if browsing_clicks >= 3:
        jump site_outlands_time

    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_compra_venta:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    show 3dgchan smiling presenting at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"Este subforo es bastante útil si tenes dinero,{w=0.5} o posesion alguna."
    show 3dgchan presenting at chan_tf_subs onlayer screens
    user"Estoy bastante interesado en negocios"
    user"Para ser honesto estaba pensando en entrar en el negocio del {b}Bathwater selling{/b},"
    user"Tengo varios frascos en venta en Mercado Libre."
    user"Incluso me estoy bañando más seguido."
    user"{fast}Soy una maquina de hacer de dinero jeje!"

    show 3dgchan disgusted at chan_tf_subs onlayer screens
    c"*agh*"
    if browsing_clicks >= 3:
        jump site_outlands_time

    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_private:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"$$$"

    if browsing_clicks >= 3:
        jump site_outlands_time

    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_discusion_general:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"El dolar se va a la mierda?{p}{w=0.5}Es la tierra plana? (Lo es){p}{w=0.5}Murio Mirtha Legrand?"
    c"Éstas preguntas y otros debates inconsecuentes los podes debatir acá, por la eternidad."

    if browsing_clicks >= 3:
        jump site_outlands_time

    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_empleos:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"Te podemos ayudar un buscar un laburito"
    c"Conozco un chico que se hizo rico con {color=[color_main]}AMWAY{/color}"
    c"Ah lo sumo te quedan unas buenas cajas de desodorante bolita."
    user"mmmm"

    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_autos:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"Ahh autos{w=0.5} los caballos de nuestra era,"
    c"Imaginate ser un plebeyo inmundo y hacer {i}trekking{/i} de un lugar a otro"
    c"Dejando de lado, mmm...a donde irías? o sea, tenes la rotíseria a un par de cuadras."
    c"Tendrías que pagar estacionamiento...ehh"
    c"Ni hablar de que instantaneamente atropellarías a un huérfano en silla de ruedas.{p}Tenés aura de siniestro."
    c"En fin {cps=*2}SON DEMASIADAS DUDAS!,{/cps}{w} te viene bien caminar igual, {fast}esas pitusas no se sintetizan en ATP solas!!"

    user"(ಠ_ಠ)"

    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_deportes:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"Necesitas moverte para esto"
    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_cine:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(0.2)

    c"Marvel?{w} o DC?{w}"
    c"crea tu thread para discutir sobr-{nw}"
    show 3dgchan bothered at chan_tf_subs onlayer screens
    extend "{cps=100}AH DIOS PARA QUE ME GASTO{/cps}"
    hide 3dgchan at chan_tf_subs onlayer screens
    jump site_label_hide

label site_label_sexualidad:
    $browsing_clicks += 1
    show screen site_screen(isLogged, browsing_clicks)
    show screen db_screen
    $ renpy.pause(2.0, hard='True')
    show 3dgchan at chan_tf_subs onlayer screens with Dissolve(2.0)
    $ renpy.pause(4.0, hard='True')

    show 3dgchan laughing at chan_tf_subs onlayer screens
    hide 3dgchan laughing onlayer screens with Dissolve(3.0)
    $ renpy.pause(3.0, hard='True')
    jump site_label_hide
