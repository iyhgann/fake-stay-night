

# image site_inbox_img:
#     "siteinbox"
#     zoom 1
#     xalign 0.5
#     ycenter 0.5

# screen site_inbox_screen():
#     tag inbox
#     zorder 0
#     modal False
#
#     side "c r":
#         viewport id "vp_inbox":
#             mousewheel True
#             arrowkeys True
#
#             frame:
#                 background Solid("#24262c")
#                 xfill True
#
#                 vbox:
#                     xalign 0.5
#                     add "site_inbox_img"
#                     text "[name]":
#                         font gui.text_font_bold
#                         yoffset -930
#                         xoffset 190
#                         xanchor 0.5
#                         color "#ffb84e"
#                         size 16
#                         kerning -1
#                     imagebutton auto "bg/inbox_msg_%s.png":
#                         xpos 324
#                         yoffset -953
#                         action [Play("sound", "audio/nt_click_1.mp3"),  Jump('site_inbox_x_intro')]
#
#         vbar value YScrollValue("vp_inbox"):
#             unscrollable "hide"
#
#     fixed:
#         add "site_nav_logged"

screen test_screen():
    tag screen
    modal False

    viewport id "vp_test":

        frame:
            background Solid("#24262c")
            xalign 0.5
            yalign 0.5

            vbox:
                text "[test_var]":
                    font gui.text_font_bold
                    color "#ffb84e"
                    size 16
                    kerning -1

                showif test_var == "he camviado de nuevo":
                    imagebutton auto "bg/replypost_%s.png":
                        action [Play("sound", "audio/nt_click_1.mp3"),  Jump('site_outlands_replies_read')]
                        xpos 1081
                        yoffset -2
                showif test_var == "he camviado":
                    imagebutton auto "bg/replymsg_%s.png":
                        xpos 1122
                        yoffset -3
                        action [Play("sound", "audio/nt_click_1.mp3"),  Jump('site_outlands_go_inbox')]

                if foo:
                    text "THRU":
                        font gui.text_font_bold
                        color "#ffb84e"
                        size 16
                        kerning -1
                else:
                    text "FALSE":
                        font gui.text_font_bold
                        color "#ffb84e"
                        size 16
                        kerning -1

label test:
    scene black

    show screen test_screen
    show screen db_screen

    user"test"

    $test_var = "he camviado"

    user"asdasd"

    $test_var = "he camviado de nuevo"

    user"foo true next"

    $foo = True

    user"foo false next"

    $foo = False

    user"ASDASDAS"

    return
