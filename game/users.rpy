# side image slide in
# https://www.renpy.org/doc/html/side_image.html
transform change_transform(old, new):
    contains:
        old
        yalign 0.9
        xpos 0.0 xanchor 0.0
        linear 0.2 xanchor 1.0
    contains:
        new
        yalign 0.9
        xpos 0.0 xanchor 1.0
        linear 0.2 xanchor 0.0

# to maintain thumbnail same position
transform same_transform(old, new):
    contains:
        new
        yalign 0.9
    contains:
        new
        yalign 0.9

define config.side_image_same_transform = same_transform
define config.side_image_change_transform = change_transform

# sounds
init python:
    def key_type(event, **kwargs):
        if event == "show":
            renpy.sound.play("audio/type_1.ogg", channel="beep", loop="True")
        elif event == "slow_done" or event == "end":
            renpy.sound.stop(channel="beep")
    def key_type_medium(event, **kwargs):
        if event == "show_done":
            renpy.music.play("audio/type_medium.ogg", channel="beep", loop=True)
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")
    def rouge_type(event, **kwargs):
        if event == "show":
            renpy.sound.play("audio/rouge_type.ogg", channel="beep")
    def phone_type(event, **kwargs):
        if event == "show":
            renpy.sound.play("audio/type_phone.ogg", channel="beep", loop="True")
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")

# init python:
#     def like_perro_evt(event, interact=True, **kwargs):
#         if not interact:
#             return
#
#         if event == "end":
#             renpy.call("like_perro_label")
#

# like perro
# label like_perro_label:
#     show like_perro onlayer screens:
#         xalign .4 yalign .7 subpixel True
#         parallel:
#             linear 1.0 yalign .6
#         parallel:
#             linear 1.0 alpha 0
#
#     with dissolve
#     return

# users
image side azure:
    # yoffset -50
    # xoffset 100
    "users/azure.png"

image side cacop:
    "users/cacop.jpg"
image side deedeemuss:
    "users/deedeemuss.png"
image side oda:
    "users/oda.jpg"
image side edroh:
    "users/edroh.jpg"
image side emrz:
    "users/emrz.jpg"
image side kom:
    "users/kom.jpg"
image side flaquito:
    "users/flaquito.jpg"
image side ken2:
    "users/ken2.png"
image side rouge:
    "users/rouge.png"
image side sidon = Movie(play="users/sidon.webm")
image side robbie = Movie(play="users/robbie.webm")
image side spon:
    "users/spon.png"
image side helpmehelpyou:
    "users/helpmehelpyou.jpg"
image side ibrn:
    "users/ibrn.jpg"
image side bluesky:
    "users/bluesky.jpg"


define azure = Character("~AzureWratH", image="azure", callback=key_type)
define cacop = Character("cacop", image="cacop", callback=key_type)
define deedeemuss = Character("deedeemuss", image="deedeemuss")
define oda = Character("Oda Al Sol", image="oda", callback=key_type)
define edroh = Character("Edroh-7007", image="edroh", callback=key_type_medium)
define emrz = Character("EMRZ", image="emrz", callback=key_type)
define kom = Character("-KoM-", image="kom", callback=key_type)
define flaquito = Character("FLAQUITOSEXI", image="flaquito", callback=key_type)
define ken2 = Character("ken2", image="ken2", callback=phone_type)
define rouge = Character("Rouge.", image="rouge", callback=rouge_type)
define sidon = Character("Sidon", image="sidon", callback=key_type)
define robbie = Character("RobbieRabbit", image="robbie", callback=key_type)
define spon = Character("Spony", image="spon", callback=key_type_medium)
define helpmehelpyou = Character("helpmehelpyou", image="helpmehelpyou", callback=phone_type)
define ibrn = Character("IBRN", image="ibrn", callback=key_type_medium)
define bluesky = Character("BlueSky", image="bluesky", callback=key_type_medium)
