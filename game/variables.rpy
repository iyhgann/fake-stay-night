init python:
    def beepy_voice_zelda(event, interact=True, **kwargs):
        beepy_voice_internal( event, interact, "audio/beep_1_.wav")

    def check_for_tags(s):

        # {fast} short-circuits the gradual talk, printing it "now"
        # Anything before a {fast} must therefor be removed
        if "{fast}" in s:
            s = s.split("{fast}")
            s = s[-1]

        # These will be used for padding (they give a time during which the text will stop)
        if "{w" in s:
            s = s.replace("{w", " |##")

        if "{p" in s:
            s = s.replace("{p", " |##")

        s = s.replace("}", "} ")

        return s

    def beepy_voice_internal(event, interact, beepy_voice_theSfx):
        global _last_beeped_text

        if not interact:
            return

        if event == "show_done":
            # Using _last_raw_what makes the callback compatible with extend character.
            # Fair warning: _last_raw_what is undocumented and subject to change.
            s = _last_raw_what
            speed = float(_preferences.text_cps)

            sfx = beepy_voice_theSfx

            if s and speed:
                import re

                intervals = []
                padding = 0

                if not _last_beeped_text:
                    if "{fast}" in s or "{w" in s or "{p" in s:
                        s = check_for_tags(s)

                    s = re.sub(r"{.+?} ", "", s) #Instead of capturning "{something}" we had to capture "{something} "

                    # Whenever we encounter a click-to-continue tag, split the string.
                    _last_beeped_text = s.split("|##}")

                #words = _last_beeped_text.pop(0).split() # We don't use words anymore!!!
                characters = _last_beeped_text.pop(0)

                i = 0
                while i < len( characters ):
                    # If number of seconds is provided, add it to interval.
                    if characters[i:].startswith( "|##=" ) :

                        i += 4
                        sizeOfNumber = 0;
                        while characters[i + sizeOfNumber] in "0123456789,." :
                            sizeOfNumber += 1;
                        try:
                            padding += int(characters[i:i + sizeOfNumber])
                        except ValueError:
                            padding += float(characters[i:i + sizeOfNumber])

                        i += sizeOfNumber
                        continue

                    if characters[i] in ",. ":
                        if ( intervals ) : # not empty
                            intervals[-1] += 1/speed
                        else:
                            padding += 1
                        i += 1;
                        continue

                    intervals.append((1+padding)/speed)
                    padding = 0;
                    i += 1;
                renpy.show_screen("silly_timers", intervals, sfx)

        elif event == "slow_done":
            renpy.hide_screen("silly_timers")

define _last_beeped_text = []

screen silly_timers(intervals, sfx):
    default play_time = 0.001 # We play the very first sound asap.

    for i in intervals:
        timer play_time action Play("voice", sfx)
        $ play_time += i

define type_medium_array = ["audio/type_medium.ogg", "audio/type_medium_2.ogg"]

init python:
    def beep_1(event, **kwargs):
        if event == "show_done":
            renpy.music.play("audio/beep_1_.wav", channel="beep", loop=True)
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")

    def beep_2(event, **kwargs):
        if event == "show_done":
            renpy.music.play("audio/beep_2.ogg", channel="beep", loop=True)
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")

    def beep_onga(event, **kwargs):
        if event == "show_done":
            renpy.music.play("audio/beep_3.ogg", channel="beep", loop=True)
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")

    def beep_female(event, **kwargs):
        if event == "show_done":
            renpy.music.play("audio/beep_female.wav", channel="beep", loop=True)
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")

    def x_type(event, **kwargs):
        if event == "show_done":
            renpy.music.play(renpy.random.choice(type_medium_array), channel="beep", loop=True)
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")

    def x_type_hard(event, **kwargs):
        if event == "show_done":
            renpy.music.play("audio/type_hard.ogg", channel="beep", loop=True)
        elif event == "slow_done" or event == "end":
            renpy.music.stop(channel="beep")

define centered = Character(None, what_xalign=0.5, what_yalign=0.5, window_xalign=0.5, window_yalign=0.5, window_background=None, callback=beep_1)
define top = Character(None, what_xalign=0.5, what_yalign=0.5, window_xalign=0.5, window_yalign=0.2, window_background=None, callback=beep_1)
define right = Character(None, what_xalign=0.8, what_yalign=0.5, window_xalign=0.9, window_yalign=0.2, window_background=None, callback=beep_1)
define left = Character(None, what_xalign=0.2, what_yalign=0.5, window_xalign=0.2, window_yalign=0.4, window_background=None, callback=beep_1)
define bottom_right = Character(None, what_xalign=0.8, what_yalign=0.8, window_xalign=0.2, window_yalign=0.8, window_background=None, callback=beep_1)
define narrator = Character(None, what_prefix='{cps=45}', callback=beep_1)
define user = Character("[name]", what_prefix='{cps=55}', callback=beep_1)
define who = Character("?????", what_prefix='{cps=75}', callback=beep_female)
define c = Character("3DG Chan", what_prefix='{cps=75}', callback=beep_female)
define x = Character("X", callback=x_type)
define xh = Character("X", callback=x_type_hard)
define onga = Character("Onganía", what_prefix = '{cps=40}', callback=beep_onga)
define m = Character("Mario", what_font="fonts/joystix monospace.ttf", what_size=18, kerning=1.1)
define userpost=Character('me presento xD', kind=nvl, color="#000000", who_size=16, callback=key_type)

# colors
define color_main = "#f1921a"
define color_fire = "#bf1010"
define font_augustus = "fonts/AUGUSTUS.ttf"

# game variables
define name = "User"
define sex = ""
define SEX_MALE = "1"
define SEX_FEMALE = "2"

# flags site
define can_log = False
define isLogged = False

# flags outlands
define posted = False
define replies = False
define messages = False
define postRepliesNumber = 0
define postViewsNumber = 0
define choosed_porn = False
define choosed_girl = False
define choosed_max_exclamations = False
define choosed_chilean = False
define choosed_voxed = False
define choosed_trava = False

define test_var="asdasd"
define foo=False

# music
# define audio.site_music = "audio/front_mission_network.mp3"
define audio.site_music = "audio/steins_gate_laboratory.mp3"
define audio.posting_music = "audio/sims.mp3"
define audio.decalogue_music = "audio/o_fortuna.ogg"
define audio.reading_posts_music = "audio/max_payne.mp3"

# images transitions
image gh_licha = "god_hand/licha.png"
image gh_sinon = "god_hand/sinon.png"
image gh_dia = "god_hand/dia.png"
image gh_ahiru = "god_hand/ahiru.png"
image gh_jeri = "god_hand/jeri.png"

define flash = Fade(.25, 0.0, .75, color="#fff")
image white = Solid("#fff")

# custom fonts
define augustus_font = "fonts/AUGUSTUS.ttf"
