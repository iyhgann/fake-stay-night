image preloader = Movie(play="bg/preloader.webm", mask="bg/preloader_mask.webm")
image ongania dark = im.Blur(im.MatrixColor("characters/ongania.png", im.matrix.desaturate() * im.matrix.tint(0.1, 0.0, 0.1)), 1.3)

style end_sub_text_style is image:
    size 40
    color "#f1921a"

image end_sub_text = Text("PARA CONTINUAR", style="end_sub_text_style")

image not_found_img:
    "not_found"
    zoom 0.8
    xalign 0.5
    yalign 0.5

screen not_found:
    frame:
        background Solid("#202124")
        xfill True
        yfill True
        add "not_found_img"

label end:
    scene black with dissolve
    play sound "audio/netbook_turn_off.ogg"
    $ renpy.pause(2.0, hard='True')

    play ambient 'audio/street_scene.mp3' fadein 2.0
    $ renpy.pause(3.0, hard='True')

    user"Que extraño fue todo eso"

    scene room bed with dissolve

    user"Cuando lo pienso no tiene sentido"
    user"Pero estoy seguro de lo que tengo que hacer{w=0.2}.{w=0.5}.{w=0.5}."

    user"{cps=*3}TENGO QUE CONSEGUIR EL IG DE ESA PENDEJA :D!!!"

    play sound 'audio/door_mom_2.ogg'

    if sex == SEX_FEMALE:
        "Nena te buscan!!!" with hpunch
    else:
        "Nene te buscan!!!" with hpunch

    user"¿Ehh?{w} ¿una {i}visita{/i}?"

    scene black with dissolve
    play sound "audio/footsteps.ogg"
    $ renpy.pause(4.0, hard='True')
    play music 'audio/btf_suspense.ogg' noloop
    scene door closed with dissolve
    user"¿La puta madre justo ahora quién podra ser?"

    scene black with dissolve
    play sound 'audio/keys.ogg'

    pause 5.0


    show door_open_fg
    show ongania dark behind door_open_fg with dissolve:
        xalign 0.0
        yalign 1.0
        zoom 0.7
        ease_quad 2.0 xalign 0.2
    show door open bg behind ongania

    stop sound fadeout 1.0

    user"Ehp{w=0.2}...Sí?"

    stop ambient fadeout 2.0

    show ongania with dissolve

    onga"{cps=*3}[name] estas vivo!" with hpunch
    play music 'audio/btf_end_2_1.ogg' fadein 1.0
    user"Perdon?"

    onga"Se ve que todavía no me conoces en esta línea de tiempo{p}Soy el General {i}{color=[color_main]}Juan Carlos Onganía{/color}.{/i}"

    onga"Está Usted diferente"
    onga"ejem...{w=0.5}{i}ha estado mucho tiempo bajo el sol?{/i}"

    user"{i}Perdon???{/i}"

    onga"Tenemos que volver, [name]"

    user"Disculpa pero no entiendo de que me estas hablan{nw}"

    onga"{cps=*2}EL FORO, {w} Tienes que seguirme, esta vez lo haremos todo correctamente." with hpunch

    user"Ok, ma!!!...MAMAAAAAAA!{nw}" with hpunch

    onga"Ya le avise inverbe soez, VAMOS"

    play music 'audio/btf_end_2_2.ogg' fadein 1.0 noloop
    scene black with Dissolve(1.0)
    show image 'bg/btf.png':
        xalign 0.5 yalign 0.5
        easein 5.0 zoom 0.3

    show end_sub_text:
        alpha 0.0 xalign 0.5 yalign 0.7
        pause 5.0
        easeout 1.0 alpha 1.0

    $ renpy.pause(9.0, hard='True')
    stop music
    show preloader at truecenter
    $ renpy.pause(1.0, hard='True')

    call screen not_found
    $ renpy.pause(2.0, hard='True')
    show screen db_screen

    return
