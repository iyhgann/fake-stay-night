image site_inbox_img:
    "siteinbox"
    zoom 1
    xalign 0.5
    ycenter 0.5

screen site_inbox_screen():
    tag site
    zorder 0
    modal False

    side "c r":
        viewport id "vp_inbox":
            mousewheel True
            arrowkeys True

            frame:
                background Solid("#24262c")
                xfill True

                vbox:
                    xalign 0.5
                    add "site_inbox_img"
                    text "[name]":
                        font gui.text_font_bold
                        yoffset -930
                        xoffset 190
                        xanchor 0.5
                        color "#ffb84e"
                        size 16
                        kerning -1
                    imagebutton auto "bg/inbox_msg_%s.png":
                        xpos 324
                        yoffset -953
                        action [Play("sound", "audio/nt_click_1.mp3"),  Jump('site_inbox_x_intro')]

        vbar value YScrollValue("vp_inbox"):
            unscrollable "hide"

    fixed:
        add "site_nav_logged"

label site_inbox:
    scene black
    show screen site_inbox_screen with dissolve
    show screen db_screen
    $ renpy.pause(1.0, hard='True')

    user"Mmm, que mierda es esto??{p}De {color=[color_main]}{size=+10}x{/size}{/color}???{w} será una mina??"
    user"Este foro tiene pinta de estar lleno de mujeres!!! :D"

    hide screen db_screen
    call screen site_inbox_screen

label site_inbox_x_intro:
    show screen site_inbox_screen
    show screen db_screen

    x"Te estaba esperando"

    play sound 'audio/lightswitch.ogg'
    hide screen site_inbox_screen
    hide screen db_screen

    $ renpy.pause(3.0, hard='True')
    play ambient "audio/wind.ogg"

    user"¿Eh?"

    scene black
    #foreground
    show siteinboxgradient onlayer screens:
        alpha 0.5
        choice:
            linear 0.2 alpha 0.6
            linear 0.2 alpha 0.5
        choice:
            linear 4 alpha 0.8
            linear 4 alpha 0.5
        choice:
            linear 2 alpha 0.8
            linear 1 alpha 0.5
        repeat

    show siteinboxdark with dissolve

    $ renpy.pause(1.0, hard='True')

    xh"{font=fonts/AUGUSTUS.ttf}Terminado de dictar los 10 mandamientos a Moises Dios le dijo:{p}{i}\"Éstas son mis leyes. Yo, Yaveh, te encomiendo que las lleves a tu pueblo.{/i}"
    user"{cps=*0.5}...{/cps}¿hola?"

    show x dark at x_tf onlayer screens with dissolve:
        xalign 0.8

    xh"{font=fonts/AUGUSTUS.ttf}{i}Mas una ley no te diré, {w=0.2} pues es de tu pueblo entenderla con el tiempo,{w=0.2}{cps=*5}{size=-10}y una conexion adsl de minimo 25mbps.{/i}"
    user"Si hay alguien ahí {w=0.3}no se de que estás{nw}"
    xh"{font=fonts/AUGUSTUS.ttf}{i}Esta ley {u}es y no es{/u}:{p}No es porque es la hija, concebida de aquellas que cargas.{p}Lo es porque es sino acaso la mas importante.{/i}"

    user"..."

    pause 1.0

    show x unshaven at x_tf onlayer screens with dissolve:
        xalign 0.8

    x"hi ^^"

    x"estoy segura que tenes muc{nw}"

    show x unshaven huh at x_tf onlayer screens:
        xalign 0.8

    x"*ehp*"

    show x unshaven huh at x_tf onlayer screens:
        xalign 1.0
        ease_cubic 0.5 xalign 2.0

    play sound [ "<silence 0.5>", 'audio/shaver.ogg']

    pause 7.0

    stop sound
    show x at x_tf onlayer screens:
        xalign 0.8

    x"{cps=*4.0}estoy segura que tenes muchas preguntas{/cps}"

    menu:
        "Tu forma de expresarte, es...como una princesa":
            show x remembering at x_tf onlayer screens:
            call like_perro_label from _call_like_perro_label_3
            x"lo...decis en serio?? <3<3<3<3"

        "Acaso no sos el trava el que mencionan cada 2 threads":
            show x screaming at x_tf onlayer screens:
                yanchor -0.4
                zoom 1.1
                rotate -20
            xh"{cps=*2.0}TENGO LA IGNORE EMPAPELADA CON PELOTUDITOS COMO VOS SABES? T_T" with vpunch

    hide x screaming onlayer screens
    show x at x_tf onlayer screens:
        xalign 0.8

    x"voy a empezar, lee bien lo que digo"

    jump story
